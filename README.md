# Donnée Sécheresse 🐪 - Propluvia 🌦️

[![pipeline status](https://gitlab.com/dashboard-streamlit/propluvia/badges/main/pipeline.svg)](https://gitlab.com/dashboard-streamlit/propluvia/-/commits/main)
[![coverage report](https://gitlab.com/dashboard-streamlit/propluvia/badges/main/coverage.svg)](https://gitlab.com/dashboard-streamlit/propluvia/-/commits/main)

[![Latest Release](https://gitlab.com/dashboard-streamlit/propluvia/-/badges/release.svg)](https://gitlab.com/dashboard-streamlit/propluvia/-/releases)

[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit.ci status](https://results.pre-commit.ci/badge/github/asottile/dead/main.svg)](https://results.pre-commit.ci/latest/github/asottile/dead/main)

## Description
Les données manipulées concernent les restrictions liées à la sécheresse.
Ces données sont publiées sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/donnee-secheresse-propluvia/).
Vous trouverez la documentation dans la description, et dans les métadonnées des différents fichiers.
On sʼintéresse ici particulièrement aux jeux de données
**Zones d'alerte** et **Arrêtés**, même si les autres jeux de données peuvent être utiles pour des visualisations par exemple.

## Visuals
![presentation of app](./images/view.gif)

## Installation
- [ ] [Installation de docker compose 🐳](https://docs.docker.com/compose/install/)
- [x] Modifier **si nécessaire** les variables d'environnement sur le [fichier .env](./.env)

## Usage
1. Dans un terminal `docker compose up`
1. 🖱️ Cliquer sur Ctrl + clic sur l'URL affichée.

## Support
Possibilité d'ouvrir une issue sur [le repo](https://gitlab.com/dashboard-streamlit/propluvia/-/issues)

## Roadmap
* Partie 1 - Ingénierie
    * Construire une pipeline de données pour ingérer ces fichiers pour une analyse ultérieure.
    * Construire une configuration (par exemple, avec Docker) avec :
        - [x] un orchestrateur
            - [x] docker compose 🐳
                - [x] PostgreSQL/Postgis
                - [x] Application Streamlit 📈
        - [x] un entrepôt de données local (par exemple, une base de données PostgreSQL)
            * PostgreSQL avec une extention Postgis
        - [x] une tâche pour ingérer le(s) fichier(s) dans l'entrepôt de données
            - [x] bouton dans l'interface
            - [ ] ajouté un scheduler
        - [x] le moteur de visualisation de votre choix
            - [x] Application Streamlit 📈
            - [ ] envie de check superset
* Partie 2 - Analyse
    - [x] Data exploration 🔍
        - [x] Lecture doc sur [data.gouv.fr donnee-secheresse-propluvia](https://www.data.gouv.fr/fr/datasets/donnee-secheresse-propluvia/)
    * Mettre en place un tableau de bord pour suivre lʼévolution des mesures de restrictions sur le territoire et dans le temps.
    * Créer des visualisations:
        - [x] 1 ou 2 graphes au choix pour suivre la situation à une date donnée. Par exemple:
            - [x] Le nombre de département par niveau dʼalerte (considérez le niveau dʼalerte le plus élevé sur toutes les zones dʼun département).
            - [x] La répartition des restrictions sur le territoire (par zone ou par département)
        - [x] 1 ou 2 graphes au choix pour analyser lʼhistorique, Par exemple:
            - [x] Lʼévolution de la durée des arrêtés de restriction dans le temps
            - [x] Lʼévolution de la superficie française concernée par des niveaux de gravité (pour les eaux superficielles uniquement)

        - [ ] Expliquez ce que vous voyez et ce que vous apprenez
* Partie 3 - Integrer d'autres données
    - [ ] [Nombre de piscines par communes](https://www.data.gouv.fr/fr/datasets/nombre-de-piscines-par-communes-extrait-du-plan-cadastral/#/community-reuses)
    - [ ] [Communes de france - Base des codes postaux ](https://www.data.gouv.fr/fr/datasets/communes-de-france-base-des-codes-postaux/)
    - [ ] Info parcelles agricoles

## Auteures
* [Auteures](./CONTRIBUTING.md)

## License
Projet open source avec [licence](./LICENSE)
