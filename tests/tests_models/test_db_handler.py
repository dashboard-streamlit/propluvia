import unittest
from pathlib import PurePath
from unittest.mock import MagicMock, patch

import geopandas as gpd
import pandas as pd
from sqlalchemy import Engine

from src.models.db_handler import PgHandler


class TestGetPostgreSQLEngine(unittest.TestCase):
    def test_get_postgresql_engine_with_correct_credentials(self):
        env_path = PurePath("tests", "config", ".env")
        engine = PgHandler.get_postgresql_engine(env_path=env_path)

        # Expected connection URL
        expected_url = (
            "postgresql://postgres_user:***@postgres.example.com:5432/my_database"
        )

        # Assert engine creation and connection URL
        self.assertIsInstance(engine, Engine)
        self.assertEqual(str(engine.url), expected_url)

    def test_select_sql_query_simple_generates_select_all_query(self):
        query = PgHandler.select_sql_query_simple("users")
        expected_query = "SELECT * FROM users "
        self.assertEqual(query, expected_query)

    def test_select_sql_query_simple_generates_select_specific_columns(self):
        columns = ["name", "email"]
        query = PgHandler.select_sql_query_simple("orders", columns)
        expected_query = "SELECT name, email FROM orders "
        self.assertEqual(query, expected_query)

    def test_select_sql_query_simple_handles_empty_column_list(self):
        query = PgHandler.select_sql_query_simple("products", [])  # Empty list
        expected_query = "SELECT * FROM products "
        self.assertEqual(query, expected_query)

    @patch("geopandas.GeoDataFrame.to_postgis")
    @patch.object(PgHandler, "get_postgresql_engine")
    def test_save_geojson_to_postgis(self, mock_get_engine, mock_to_postgis):
        # Mock GeoDataFrame methods and engine
        mock_engine = MagicMock()
        mock_to_postgis.return_value = mock_engine

        # Create test GeoDataFrame
        geo_df = gpd.GeoDataFrame(
            {"geometry": [MagicMock()]}
        )  # Mock geometry for simplicity

        # Call the function
        PgHandler.save_geojson_to_postgis(geo_df, "my_table")

        # Expected call to PgHandler.get_postgresql_engine
        mock_get_engine.assert_called_once_with()  # Remove arguments from assertion

        # Expected call to GeoDataFrame.to_postgis (unchanged)
        mock_to_postgis.assert_called_once_with(
            "my_table", mock_get_engine.return_value, if_exists="replace"
        )

    @patch.object(PgHandler, "select_sql_query_simple")
    @patch.object(PgHandler, "get_postgresql_engine")
    @patch("geopandas.read_postgis")
    def test_read_geojson_from_postgis_simple(
        self, mock_read_postgis, mock_get_engine, mock_select_query
    ):
        # Mock dependencies
        mock_engine = MagicMock()
        mock_get_engine.return_value = mock_engine
        mock_select_query.return_value = "SELECT * FROM my_table"
        mock_df = MagicMock(spec=gpd.GeoDataFrame)
        mock_read_postgis.return_value = mock_df

        # Call the function
        df = PgHandler.read_geojson_from_postgis("my_table")

        # Expected call to GeoPandas.read_postgis
        mock_read_postgis.assert_called_once_with(
            mock_select_query.return_value, mock_engine, geom_col="geometry"
        )

        # Expected logging message
        self.assertLogs("read from my_table", level="INFO")

        # Return value check
        self.assertEqual(df, mock_df)

    @patch.object(PgHandler, "select_sql_query_simple")
    @patch.object(PgHandler, "get_postgresql_engine")
    @patch("geopandas.read_postgis")
    def test_read_geojson_from_postgis_specific_columns_and_geometry(
        self, mock_read_postgis, mock_get_engine, mock_select_query
    ):
        # Mock dependencies
        mock_engine = MagicMock()
        mock_sql_query = "SELECT name, shape FROM regions"
        mock_get_engine.return_value = mock_engine
        mock_select_query.return_value = mock_sql_query
        mock_df = MagicMock(spec=gpd.GeoDataFrame)
        mock_read_postgis.return_value = mock_df

        # Call the function with specific columns and geom_col
        df = PgHandler.read_geojson_from_postgis(
            "regions", ["name", "shape"], geom_col="shape"
        )

        # Expected call to GeoPandas.read_postgis
        mock_read_postgis.assert_called_once_with(
            mock_sql_query, mock_engine, geom_col="shape"
        )

        # Return value check
        self.assertEqual(df, mock_df)

    @patch.object(pd.DataFrame, "to_sql")
    @patch.object(PgHandler, "get_postgresql_engine")
    def test_saves_dataframe_to_postgres_with_replace(self, mock_engine, mock_to_sql):
        # Mock data
        mock_df = MagicMock(spec=pd.DataFrame)
        table_name = "my_table"
        engine = MagicMock(spec=Engine)  # Mock the engine object

        # Successful database operation
        mock_engine.return_value = engine

        # Call the function
        PgHandler.save_dataframe_to_postgres(mock_df, table_name)

        # Assertions
        mock_engine.assert_called_once_with()
        self.assertLogs(
            f"Save DataFrame to {table_name} with rule if_exists: replace 💾",
            level="INFO",
        )

    @patch.object(pd.DataFrame, "to_sql")
    @patch.object(PgHandler, "get_postgresql_engine")
    def test_raises_exception_on_database_error(self, mock_engine, mock_to_sql):
        # Mock data
        mock_df = MagicMock(spec=pd.DataFrame)
        table_name = "my_table"
        engine = MagicMock()  # Mock the engine object
        mocked_exception = Exception("Mocked database exception")

        # Simulate database error (to_sql raising an exception)
        mock_engine.return_value = engine
        mock_to_sql.side_effect = mocked_exception

        # Call the function and expect an exception
        with self.assertRaises(Exception) as cm:
            PgHandler.save_dataframe_to_postgres(mock_df, table_name)

            # Assertions
            mock_engine.assert_called_once_with()
            mock_to_sql.assert_called_once_with(
                name=table_name, con=engine, if_exists="replace"
            )
            self.assertEqual(
                str(cm.exception),
                "Error saving DataFrame to Postgres: Mocked database exception",
            )

    @patch.object(PgHandler, "get_postgresql_engine")
    @patch.object(pd, "read_sql_table")
    def test_reads_data_from_specified_table(self, mock_read_sql, mock_get_engine):
        # Mock data
        mock_engine = MagicMock(spec=Engine)
        mock_connection = MagicMock()
        mock_df = MagicMock(spec=pd.DataFrame)

        # Mock behavior
        mock_get_engine.return_value = mock_engine
        mock_engine.return_value = mock_connection
        mock_read_sql.return_value = mock_df

        # Call the function
        table_name = "my_table"
        result_df = PgHandler.read_dataframe_to_postgres_table(table_name)

        # Assertions
        mock_get_engine.assert_called_once_with()
        self.assertLogs(f"Retrieve DataFrame from {table_name}", level="INFO")
        self.assertEqual(result_df, mock_df)

    @patch.object(PgHandler, "get_postgresql_engine")
    @patch.object(pd, "read_sql_query")
    def test_read_dataframe_to_postgres_query(self, mock_read_sql, mock_get_engine):
        # Mock data
        mock_engine = MagicMock(spec=Engine)
        mock_connection = MagicMock()
        mock_df = MagicMock(spec=pd.DataFrame)

        # Mock behavior
        mock_get_engine.return_value = mock_engine
        mock_engine.return_value = mock_connection
        mock_read_sql.return_value = mock_df

        # Call the function
        sql_query = "SELECT * FROM my_table"
        result_df = PgHandler.read_dataframe_to_postgres_query(sql_query)

        # Assertions
        mock_get_engine.assert_called_once_with()
        self.assertLogs(f"Retrieve DataFrame from {sql_query}", level="INFO")
        self.assertEqual(result_df, mock_df)
