import unittest

import numpy as np
import pandas as pd

from src.models.explore_data import ExploreData


def mock_apply_func(df):
    # This is a simple mock function that just returns the sum of a column
    return df["value"].values[0] * 2


class TestExploreData(unittest.TestCase):
    def setUp(self):
        # Create a sample DataFrame with date columns
        data = {
            "date": pd.to_datetime(
                ["2023-01-01", "2023-01-02", "2023-01-03", "2023-01-04"]
            ),
            "value": [10, 20, 15, 30],
        }
        self.df = pd.DataFrame(data)

    def test_get_index_value_returns_row_for_existing_index(self):
        # Create DataFrame with sample data and index
        data = {"col1": [1, 2, 3], "col2": ["A", "B", "C"]}
        index = ["index1", "index2", "index3"]
        df = pd.DataFrame(data, index=index)
        key_to_find = "index2"

        # Call the function
        value = ExploreData.get_index_value(df.copy(), key_to_find)

        # Assertions
        expected_value = [2, "B"]

        self.assertIsInstance(value, np.ndarray)

        self.assertListEqual(value.tolist(), expected_value)  # Compare values

    def test_get_index_value_returns_default_value_for_missing_index(self):
        # Create DataFrame with sample data and index
        data = {"col1": [1, 2, 3], "col2": ["A", "B", "C"]}
        index = ["index1", "index2", "index3"]
        df = pd.DataFrame(data, index=index)
        key_to_find = "missing_index"

        # Call the function
        value = ExploreData.get_index_value(df.copy(), key_to_find)

        # Assertions
        self.assertEqual(value, 0)  # Check for default value

    def test_values_by_dates(self):
        # Define function arguments
        compute_result = "total_value"
        start_date_col = "date"
        end_date_col = "date"
        apply_func = mock_apply_func

        # Call the function
        result_df = ExploreData.values_by_dates(
            self.df.copy().reset_index(drop=True),
            compute_result,
            start_date_col,
            end_date_col,
            apply_func,
        )

        # Expected results (assuming mock_apply_func returns the sum of "value")
        expected_dates = pd.to_datetime(
            ["2023-01-01", "2023-01-02", "2023-01-03", "2023-01-04"]
        )
        expected_data = {"dates": expected_dates, compute_result: [20, 40, 30, 60]}
        expected_df = pd.DataFrame(expected_data).set_index("dates")

        # Assertions
        self.assertTrue(result_df.equals(expected_df))
        self.assertIsInstance(result_df.index, pd.DatetimeIndex)

    def test_values_by_dates_empty_dataframe(self):
        # Test with an empty DataFrame
        empty_df = pd.DataFrame(columns=["date", "value"])
        result_df = ExploreData.values_by_dates(
            empty_df,
            compute_result="total_value",
            start_date_col="date",
            end_date_col="date",
            apply_func=mock_apply_func,
        )

        # Expect an empty DataFrame with the dates column as index
        self.assertTrue(result_df.empty)
        self.assertIn("total_value", result_df.columns)
