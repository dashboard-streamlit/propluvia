import unittest

import pandas as pd

from src.models.check_data import CheckData


class TestCheckData(unittest.TestCase):
    def test_start_date_upper_end_date(self):
        # Create DataFrame with sample data
        data = {
            "id": [1, 2, 3],
            "start_date": pd.to_datetime(["2023-01-01", "2023-02-01", "2023-01-15"]),
            "end_date": pd.to_datetime(["2023-02-28", "2023-01-31", "2023-01-10"]),
        }
        df = pd.DataFrame(data)
        date_start_col = "start_date"
        date_end_col = "end_date"

        # Call the function
        invalid_df, valide_df = CheckData.start_date_upper_end_date(
            df.copy(), date_start_col, date_end_col
        )

        invalide_expected_data = {
            "id": [2, 3],
            "start_date": pd.to_datetime(["2023-02-01", "2023-01-15"]),
            "end_date": pd.to_datetime(["2023-01-31", "2023-01-10"]),
        }
        invalide_expected_df = pd.DataFrame(invalide_expected_data)

        valide_expected_data = {
            "id": [1],
            "start_date": pd.to_datetime(["2023-01-01"]),
            "end_date": pd.to_datetime(["2023-02-28"]),
        }
        valide_expected_df = pd.DataFrame(valide_expected_data)

        # Assertions
        pd.testing.assert_frame_equal(
            invalide_expected_df.reset_index(drop=True),
            invalid_df.reset_index(drop=True),
        )
        pd.testing.assert_frame_equal(
            valide_expected_df.reset_index(drop=True), valide_df.reset_index(drop=True)
        )

    def test_start_date_upper_end_date_empty(self):
        # Create DataFrame with valid date order
        data = {
            "id": [1, 2],
            "start_date": pd.to_datetime(["2023-01-01", "2023-02-01"]),
            "end_date": pd.to_datetime(["2023-02-28", "2023-03-15"]),
        }
        df = pd.DataFrame(data)
        date_start_col = "start_date"
        date_end_col = "end_date"

        # Call the function
        invalid_df, valide_df = CheckData.start_date_upper_end_date(
            df.copy(), date_start_col, date_end_col
        )

        # Assertions
        self.assertTrue(invalid_df.empty)  # Check for empty Series
        pd.testing.assert_frame_equal(df, valide_df)
