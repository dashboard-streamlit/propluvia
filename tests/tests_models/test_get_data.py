import unittest
from pathlib import Path
from unittest.mock import patch

from src.config import DL_DATA_DIR, DL_DEFAULT, DL_UPDATE, MODEL_CONFIG
from src.models.get_data import GetData
from src.models.input_output import InputOuput


class TestGetData(unittest.TestCase):
    def setUp(self):
        self.test_url = "https://www.data.gouv.fr/fr/datasets/r/bbae8ea2-4d53-4f96-b7eb-9c08f66a07c5"
        self.resource_name = "test_resource"
        self.resource = {self.resource_name: {"url": self.test_url}}

    def tearDown(self):
        pass

    def test_download_resource_successful_download(self):
        resource = self.resource.get(self.resource_name)
        with GetData.download_resource(
            self.resource_name, resource.get("url")
        ) as response:
            self.assertIn("code_departement", response.text)
            self.assertEqual(response.status_code, 200)

    def test_download_resource_caching(self):
        resource = self.resource.get(self.resource_name)
        response1 = GetData.download_resource(self.resource_name, resource.get("url"))

        # Cached download
        response2 = GetData.download_resource(self.resource_name, resource.get("url"))

        # Assert cached response is returned
        self.assertTrue(response2.from_cache)
        response1.close()
        response2.close()

    @patch.object(GetData, "uncompress_resources")
    @patch.object(GetData, "save_file_data_to_db")
    def test_download_resources(self, mock_save_data, mock_uncompress):
        config_file_path = Path("src/models/config.yaml")
        _ = InputOuput.get_data_from_yaml(
            file_path=config_file_path, key_to_get="resources_to_download"
        )
        mock_data_dir = DL_DATA_DIR
        GetData().download_resources(
            config_file_path=config_file_path,
            key_to_get=DL_UPDATE,
            dl_data_dir=DL_DATA_DIR,
        )
        mock_uncompress.assert_called_with(dl_data_dir=mock_data_dir)
        mock_save_data.assert_called_with(dl_data_dir=mock_data_dir, is_update=True)

    def test_uncompress_resources(self):
        # TODO
        pass

    def test_save_file_data_to_db(self):
        # TODO
        pass

    @patch.object(GetData, "download_resources")
    def test_orchestrates_data_acquisition_and_processing_only_update(
        self, mock_download
    ):
        # Mock data directory
        mock_data_dir = DL_DATA_DIR

        # Call the function
        instance = GetData()  # Instantiate the class
        instance.get_data(only_update=True)

        # Assertions
        mock_download.assert_called_with(
            config_file_path=MODEL_CONFIG,
            key_to_get=DL_UPDATE,
            dl_data_dir=mock_data_dir,
            is_update=True,
        )

    @patch.object(GetData, "download_resources")
    def test_orchestrates_data_acquisition_and_processing(self, mock_download):
        # Mock data directory
        mock_data_dir = DL_DATA_DIR

        # Call the function
        instance = GetData()  # Instantiate the class
        instance.get_data(only_update=False)

        # Assertions
        mock_download.assert_called_with(
            config_file_path=MODEL_CONFIG,
            key_to_get=DL_DEFAULT,
            dl_data_dir=mock_data_dir,
            is_update=False,
        )
        self.assertLogs("Global dl", level="INFO")
