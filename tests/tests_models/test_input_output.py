import tempfile
import unittest
from pathlib import Path
from unittest.mock import patch

import yaml

from src.models.input_output import InputOuput


class TestInputOuput(unittest.TestCase):
    def setUp(self):
        self.test_data = {
            "resource_to_download": [
                {"name": "zones_alerte_communes", "url": "https://example.com/data1"},
                {"name": "arretes_en_vigueur", "url": "https://example.com/data2"},
            ],
            "another_key": "some_value",
        }
        self.test_file_path = Path("test_data.yaml")
        with self.test_file_path.open("w") as f:
            yaml.dump(self.test_data, f)

    def tearDown(self):
        self.test_file_path.unlink()  # Remove the temporary test file

    def test_read_all_data(self):
        """Test reading the entire data dictionary from the YAML file."""
        data = InputOuput.get_data_from_yaml(self.test_file_path)
        self.assertEqual(data, self.test_data)

    def test_read_specific_key(self):
        """Test reading data associated with a specific key."""
        resources = InputOuput.get_data_from_yaml(
            self.test_file_path, key_to_get="resource_to_download"
        )
        self.assertEqual(resources, self.test_data["resource_to_download"])

    def test_missing_file(self):
        """Test handling a non-existent YAML file."""
        missing_file = Path("missing_file.yaml")
        data = InputOuput.get_data_from_yaml(missing_file)
        self.assertIsNone(data)

    def test_invalid_yaml(self):
        """Test handling invalid YAML content."""
        with self.test_file_path.open("w") as f:
            f.write("- : invalid_yaml_content")
        data = InputOuput.get_data_from_yaml(self.test_file_path)
        self.assertIsNone(data)

    @patch("pathlib.Path.is_file")
    def test_unzip_gz_not_a_file(self, mock_is_file):
        """Tests if the function raises an error for a non-existing file."""
        mock_is_file.return_value = False
        invalid_path = Path("non_existent_file.gz")

        with self.assertRaises(ValueError) as error:
            InputOuput.unzip_gz(invalid_path)

        self.assertEqual(str(error.exception), "Provided path does not point to a file")

    def test_unzip_gz_successful_unzip(self):
        """Tests if the function successfully unzips a .gz file."""
        temp_file = tempfile.NamedTemporaryFile()
        temp_file.write(b"This is some test data in the gz file")
        temp_file.seek(0)  # Rewind the file pointer for reading
        compressed_path = InputOuput.compress_to_gz(Path(temp_file.name))
        unzipped_path = InputOuput.unzip_gz(compressed_path)

        # Assert the unzipped file exists and has the expected content
        self.assertTrue(unzipped_path.is_file())
        with open(unzipped_path, "rb") as f:
            self.assertEqual(f.read(), b"This is some test data in the gz file")

        # Clean up the unzipped file (optional)
        compressed_path.unlink()  # Remove the unzipped file
        unzipped_path.unlink()  # Remove the unzipped file
