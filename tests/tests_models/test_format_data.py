import unittest

import pandas as pd

from src.config import DATE_FMT, SECOND_DATE_FMT
from src.models.format_data import FormatData


class TestFormatData(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_clean_column_str_values_cleans_column_values_with_lowercase(self):
        df = pd.DataFrame({"text": ["HeLlOWoRlD!", "ThisIsATest."]})
        cleaned_df = FormatData.clean_column_str_values(df, "text")

        expected_df = pd.DataFrame({"text": ["helloworld!", "thisisatest."]})
        pd.testing.assert_frame_equal(cleaned_df, expected_df)

    def test_clean_column_str_values_cleans_column_values_with_lowercase_and_replacements(
        self,
    ):
        df = pd.DataFrame({"text": ["HéLlO WoRlD!", "This is a TèST."]})
        cleaned_df = FormatData.clean_column_str_values(df, "text")

        expected_df = pd.DataFrame({"text": ["hello_world!", "this_is_a_test."]})
        pd.testing.assert_frame_equal(cleaned_df, expected_df)

    def test_clean_column_str_values_raises_value_error_for_nonexistent_column(self):
        df = pd.DataFrame({"text": ["foo", "bar"]})
        with self.assertRaises(KeyError):
            FormatData.clean_column_str_values(df, "nonexistent_column")

    def test_applies_replacement_rules_from_config(self):
        original_string = "This is a TèST."
        expected_cleaned_string = "this_is_a_test."
        # Using default config path
        cleaned_string = FormatData.clean_str_values(original_string)

        self.assertEqual(cleaned_string, expected_cleaned_string)

    def test_formats_specified_date_columns(self):
        # Create DataFrame with date column
        date_column = "date_signature"
        data = {
            date_column: ["2024-03-31", "2024-03-30", "2024-03-29"],
            "column2": [10, 20, 30],
            "column3": ["data1", "data2", "data3"],
        }
        df = pd.DataFrame(data)

        # Call the function
        # Avoid modifying original DataFrame
        result_df = FormatData().format_date_columns(df.copy())

        # Assertions
        self.assertIsInstance(result_df[date_column].iloc[0], pd.Timestamp)
        self.assertLogs(f"format to date column {date_column}", level="INFO")

    def test_handle_date_parsing_with_primary_format(self):
        # Create DataFrame with dates in the primary format
        data = {"date_col": ["2023-01-01", "2023-02-15"]}
        df = pd.DataFrame(data)

        # Call the parsing function
        parsed_df = FormatData.handle_date_parsing(df.copy(), "date_col")

        # Assertions
        self.assertIsInstance(parsed_df["date_col"].values[0], pd.Timestamp)
        self.assertEqual(
            parsed_df["date_col"].tolist(),
            pd.to_datetime(data["date_col"], format=DATE_FMT).tolist(),
        )

    def test_handle_date_parsing_with_secondary_format(self):
        # Create DataFrame with dates in the secondary format
        data = {"date_col": ["0023-01-31", "0024-01-31"]}
        df = pd.DataFrame(data)

        # Call the parsing function
        parsed_df = FormatData.handle_date_parsing(df.copy(), "date_col")

        # Assertions
        self.assertIsInstance(parsed_df["date_col"].values[0], pd.Timestamp)
        self.assertEqual(
            parsed_df["date_col"].tolist(),
            pd.to_datetime(data["date_col"], format=SECOND_DATE_FMT).tolist(),
        )

    def test_swap_start_date_end_date(self):
        # Create DataFrame with sample data
        data = {
            "start_date": ["2023-01-01", "2023-02-15"],
            "end_date": ["2023-02-28", "2023-03-31"],
            "other_column": [1, 2],
        }
        df = pd.DataFrame(data)
        date_start_col = "start_date"
        date_end_col = "end_date"

        # Call the function
        swapped_df = FormatData.swap_start_date_end_date(
            df.copy(), date_start_col, date_end_col
        )

        # Assertions
        expected_cols = {
            date_end_col: data["start_date"],
            date_start_col: data["end_date"],
        }
        # Check swapped columns
        self.assertDictEqual(
            swapped_df.drop("other_column", axis=1).to_dict(orient="list"),
            expected_cols,
        )
        # Check other column remains unchanged
        self.assertEqual(swapped_df["other_column"].tolist(), data["other_column"])
