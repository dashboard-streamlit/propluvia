import unittest
from unittest.mock import MagicMock, patch

import geopandas as gpd
import pandas as pd

from src.config import DATE_FMT, DL_DEFAULT, DL_UPDATE
from src.models.db_handler import PgHandler
from src.models.format_data import FormatData
from src.models.geojson import GeoJson


class TestGeoJson(unittest.TestCase):
    @patch.object(PgHandler, "read_geojson_from_postgis")
    @patch.object(GeoJson, "get_column_to_show")
    @patch.object(FormatData, "clean_column_str_values")
    def test_get_geojson_loads_and_cleans_geojson_successfully(
        self, mock_clean_data, mock_get_column, mock_from_postgis
    ):
        # Mock data
        table_name = "my_table"
        mock_gdf = MagicMock(spec=gpd.GeoDataFrame)
        mock_df = MagicMock(spec=pd.DataFrame)
        column_to_show = "my_column"

        # Mock successful database read and data cleaning
        mock_from_postgis.return_value = mock_df
        mock_get_column.return_value = column_to_show
        mock_clean_data.return_value = (
            mock_gdf  # Assuming it returns the same GeoDataFrame
        )

        # Call the function
        instance = GeoJson()  # Instantiate the class
        retrieved_gdf = instance.get_geojson(table_name)

        # Assertions
        mock_get_column.assert_called_once_with(name=table_name)
        mock_from_postgis.assert_called_once_with(
            table=table_name, columns=None, filters=None
        )
        self.assertEqual(retrieved_gdf, mock_gdf)

    def test_get_fields_to_show_extracts_non_geometry_fields(self):
        # Mock data
        data = {
            "col1": [1, 2, 3],
            "col2": ["a", "b", "c"],
            "geometry": [None, None, None],
        }
        gdf = gpd.GeoDataFrame(data, geometry="geometry")

        # Expected output
        expected_fields = ["col1", "col2"]

        # Call the static method
        fields_to_show = GeoJson.get_fields_to_show(gdf)

        # Assertion
        self.assertEqual(fields_to_show, expected_fields)

    @patch("yaml.safe_load")
    def test_get_column_to_show_retrieves_column_name_successfully(
        self, mock_yaml_load
    ):
        # Mock data
        resource_name = "my_resource"
        expected_column_name = "my_column"
        mock_yaml_load.return_value = {
            DL_DEFAULT: {resource_name: {"column_to_show": expected_column_name}},
            DL_UPDATE: {resource_name: {"column_to_show": expected_column_name}},
        }

        # Call the function
        instance = GeoJson()  # Instantiate the class
        retrieved_column_name = instance.get_column_to_show(resource_name)

        # Assertions
        self.assertEqual(retrieved_column_name, expected_column_name)

    @patch("yaml.safe_load")
    def test_get_column_to_show_raises_value_error_for_wrong_resource(
        self, mock_yaml_load
    ):
        # Mock data
        resource_name = "wrong_resource"
        # Wrong resources
        mock_yaml_load.return_value = {
            DL_DEFAULT: {resource_name: {"foo": "bar"}},
            DL_UPDATE: {resource_name: {"foo": "bar"}},
        }

        # Call the function and expect an exception
        instance = GeoJson()
        with self.assertRaises(ValueError) as cm:
            instance.get_column_to_show(resource_name)

            self.assertEqual(
                str(cm.exception),
                f"Any {resource_name} in resources 'name' or any 'column_to_show'",
            )

    def test_converts_timestamps_to_strings(self):
        # Create a sample GeoDataFrame with timestamp and other columns
        data = {
            "geometry": [None, None],
            "timestamp_col": pd.to_datetime(["2023-01-01", "2023-02-15"]),
            "other_col": [1, 2],
        }
        gdf = gpd.GeoDataFrame(data, geometry="geometry")

        # Call the function
        converted_gdf = GeoJson.convert_timestamps_to_string(gdf.copy())

        # Assertions
        self.assertTrue(
            all(
                pd.api.types.is_string_dtype(converted_gdf[col])
                for col in converted_gdf.select_dtypes(
                    include=[pd.api.types.DatetimeTZDtype]
                )
            ),
            msg="Not all timestamp columns converted to strings",
        )
        self.assertEqual(
            converted_gdf["timestamp_col"].tolist(),
            [f"{dt.strftime(DATE_FMT)}" for dt in data["timestamp_col"]],
            msg="Timestamps not formatted correctly",
        )
        self.assertEqual(
            converted_gdf["other_col"].tolist(),
            data["other_col"],
            msg="Other columns modified",
        )
