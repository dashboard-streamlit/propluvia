import unittest

from src.views.tools.folium import Folium


class TestFolium(unittest.TestCase):
    def test_style_function_with_matching_value(self):
        # Mock data
        feature = {"properties": {"value": 1}}
        column_to_search = "value"
        style_code = {1: {"color": "red"}}
        expected_style = {"color": "red"}

        # Create an instance of Folium
        folium = Folium()

        # Call the style_function
        style = folium.style_function(feature, column_to_search, style_code)

        # Assertion
        self.assertEqual(style, expected_style)

    def test_style_function_with_no_match(self):
        # Mock data
        feature = {"properties": {"value": 2}}
        column_to_search = "value"
        style_code = {1: {"color": "red"}}

        # Create an instance of Folium
        folium = Folium()

        # Expect a KeyError to be raised if no matching style is found
        with self.assertRaises(KeyError) as cm:
            folium.style_function(feature, column_to_search, style_code)

            # Assert the error message mentions the invalid feature value
            self.assertEqual(str(cm.exception), "Invalid feature value '2' for styling")
