import unittest
from unittest.mock import MagicMock, patch

import folium
import geopandas as gpd

from src.config import VIEW_CONFIG
from src.models.input_output import InputOuput
from src.views.tools.geojson_view import GeoJsonView


class TestGeoJsonView(unittest.TestCase):
    @patch("folium.GeoJson")
    @patch.object(InputOuput, "get_data_from_yaml")
    @patch.object(
        GeoJsonView, "style_function"
    )  # Assuming style_function is in GeoJsonView
    def test_adds_geojson_layer_with_styling_and_tooltips(
        self, mock_style_function, mock_get_data, mock_geojson
    ):
        # Mock data
        mock_map = MagicMock(spec=folium.Map)
        mock_gdf = MagicMock(spec=gpd.GeoDataFrame)
        layer_name = "My GeoJSON Layer"
        column_to_show = "value"
        fields_to_show = ["col1", "col2"]
        style_codes = {"high": {"color": "red"}, "low": {"color": "green"}}
        mock_get_data.return_value = style_codes

        # Create an instance of GeoJsonView
        geo_json_view = GeoJsonView()

        # Call the function
        modified_map = geo_json_view.add_geojson_on_map(
            mock_map, mock_gdf, layer_name, column_to_show, fields_to_show
        )

        # Assertions
        mock_get_data.assert_called_once_with(VIEW_CONFIG, column_to_show)

        # Assert the returned map object
        self.assertEqual(modified_map, mock_map)
