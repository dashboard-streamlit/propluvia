# Base image for Python environment
FROM python:3.11-slim

# Set working directory
WORKDIR /app

# Copy requirements file (adjust the path if needed)
COPY requirements.txt ./

# Install dependencies
RUN pip install -r requirements.txt

# Copy your Streamlit application code (adjust the path if needed)
COPY src/ src/

# Load environment variables from .env file
ENV $(cat .env)

# Expose port based on APP_PORT from .env (default 8501)
# Use default 8501 if APP_PORT is not set
EXPOSE ${APP_PORT:-8501}

# Start the Streamlit app
CMD ["python", "-m", "streamlit", "run", "src/views/propluvia.py"]
