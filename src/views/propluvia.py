import logging

import streamlit as st

from src.config import LOGGING_LEVEL
from src.views.pages.propluvia.sidebar import show_sidebar
from src.views.tools.common import get_readme, set_page_config

logging.basicConfig(
    level=LOGGING_LEVEL,
    format="%(asctime)s %(levelname)s: %(message)s",
    datefmt="%Y/%m/%d %I:%M:%S %p",
)


def main():
    set_page_config("Propluvia")
    get_readme(["src", "views"])

    st.page_link("propluvia.py", label="Index", icon="🏠")
    st.page_link("pages/1_dashboard.py", label="Dashboard", icon="📈")
    st.page_link("pages/cartes.py", label="Cartes", icon="🗺️")
    st.page_link("pages/historique.py", label="Historique", icon="📜")

    show_sidebar()


if __name__ == "__main__":
    logging.info("Re-starting propluvia page")
    main()
