from src.views.pages.historique.main_content import HistoryMainContent
from src.views.pages.historique.sidebar import show_sidebar
from src.views.tools.common import set_page_config

set_page_config("Historique")
side_bar_infos = show_sidebar()
get_readme_path = ["src", "views", "pages", "historique"]
dashboard_main_content = HistoryMainContent(
    side_bar_infos=side_bar_infos, get_readme_path=get_readme_path
)
dashboard_main_content.show_main_content()
