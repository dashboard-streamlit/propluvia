from dataclasses import dataclass
from typing import Tuple

import pandas as pd
import streamlit as st

from src.models.db_handler import PgHandler
from src.models.explore_data import ExploreData
from src.views.tools.main_content import MainContent


@dataclass
class AlertByDate:
    """
    Class AlertByDate to get data and widget related to data for specific
    date range.
    """

    @st.cache_data
    def get_arretes_en_vigueur(self, side_bar_infos: dict) -> pd.DataFrame:
        """
        Retrieves data on regulations (arrêtés) in effect within a specified date range.

        This method retrieves data from a PostgreSQL database using the provided
        'side_bar_infos' dictionary. It expects the dictionary to contain keys
        'start_date' and 'end_date' representing the desired date
        range for filtering regulations.

        The method constructs a SQL query that:

        1. Selects specific columns from the 'arretes' and 'zones_alerte' tables.
        2. Performs an inner join between the tables based on the 'id_zone' column.
        3. Filters the 'arretes' table based on the 'debut_validite_arrete'
            (start validity date) being greater than or equal to the provided
            'start_date' and the 'fin_validite_arrete'
            (end validity date) being less than or equal to the provided 'end_date'.

        The retrieved data is then loaded into a pandas DataFrame and returned.

        Args:
            side_bar_infos (dict): A dictionary containing keys 'start_date'
            (string) and 'end_date' (string) specifying the date
            range for filtering regulations.

        Returns:
            pd.DataFrame: A pandas DataFrame containing information on
                regulations (arrêtés) in effect within the specified date range.
                The DataFrame includes columns like:
                    'id_arrete' (regulation ID),
                    'code_departement' (department code),
                    etc...
        """
        choose_date = side_bar_infos.get("choose_date")
        start_date_col = "debut_validite_arrete"
        end_date_col = "fin_validite_arrete"
        sql_query = f"""
        SELECT
            a.{start_date_col}, a.{end_date_col},
            a.id_arrete, a.id_zone, a.numero_niveau, a.nom_niveau,
            z.code_departement, z.nom_departement
        FROM arretes a
        INNER JOIN zones_alerte z ON a.id_zone = z.id_zone
        WHERE a.{start_date_col}  <= '{choose_date}'
        AND a.{end_date_col}  >=  '{choose_date}'
        """
        df = PgHandler.read_dataframe_to_postgres_query(sql_query)
        return df

    @staticmethod
    def get_departement_by_level_max(
        df: pd.DataFrame,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Identifies departments with the maximum level and groups departments
        by maximum level.

        Args:
            df (pd.DataFrame): The DataFrame containing information about
            departments and levels.
                - Assumed to have columns 'nom_departement' and 'nom_niveau'.

        Returns:
            Tuple[pd.DataFrame, pd.DataFrame]: A tuple containing two DataFrames:
                - highest_level_per_dept: DataFrame with departments and their
                maximum levels.
                - nb_dept_by_level_max: DataFrame showing the count of departments
                per maximum level.
        """
        level_name = "nom_niveau"
        dept_name = "nom_departement"
        highest_level_per_dept = pd.DataFrame(
            df.groupby(dept_name)[level_name].max()
        ).reset_index()
        nb_dept_by_level_max = highest_level_per_dept.groupby(level_name).count()
        return highest_level_per_dept, nb_dept_by_level_max

    def show_level_metrics(self, df):
        """
        Displays metrics for different levels (vigilance, alerte, etc.).

        This method assumes the DataFrame contains information about levels
        (e.g., vigilance, alerte) and likely has these columns:
            'l_vigilance', 'l_alerte', 'l_alerte_renforcee', 'l_crise'.
        Display metrics for each level.

        Args:
            df (pd.DataFrame): The DataFrame containing level information.
        """
        l_vigilance, l_alerte, l_alerte_renforcee, l_crise = st.columns(4)
        with l_vigilance:
            name = "Vigilance"
            value = ExploreData.get_index_value(df, name)
            st.metric(name, value)
        with l_alerte:
            name = "Alerte"
            value = ExploreData.get_index_value(df, name)
            st.metric(name, value)
        with l_alerte_renforcee:
            name = "Alerte renforcée"
            value = ExploreData.get_index_value(df, name)
            st.metric(name, value)
        with l_crise:
            name = "Crise"
            value = ExploreData.get_index_value(df, name)
            st.metric(name, value)

    def show_departement_by_level(
        self, df: pd.DataFrame, side_bar_infos: dict, style: dict
    ):
        """
        Displays department information grouped by their maximum level.

        This method analyzes a DataFrame and displays information about
        departments and their maximum levels.

        Args:
            df (pd.DataFrame): The DataFrame containing information
                about departments and levels.
            side_bar_infos (dict): A dictionary containing user choice.
            style (dict): A dictionary containing styling options.
        """
        highest_level_per_dept, nb_dept_by_level_max = (
            self.get_departement_by_level_max(df.copy())
        )
        st.subheader(
            "Nombre de départements part niveaux ",
            divider=style.get("divider"),
            help="""
            Nombre de départements par niveau d'alerte
            (considérer le niveau d'alerte le plus élevé sur toutes les zones d'un département).
            """,
        )
        self.show_level_metrics(nb_dept_by_level_max)

        if side_bar_infos.get("show_df"):
            df_col1, df_col2 = st.columns(2)
            with df_col1:
                st.subheader(
                    "Liste de département avec leur niveau max",
                    divider=style.get("divider"),
                )
                st.dataframe(highest_level_per_dept, hide_index=True)
            with df_col2:
                st.subheader(
                    "Tableau des niveau avec la somme des départements",
                    divider=style.get("divider"),
                )
                st.write(nb_dept_by_level_max)


@dataclass
class DashboardMainContent(MainContent):
    """Class DashboardMainContent for all widget on the dashboard main content."""

    def show_main_content(self):
        """
        Displays the main content of the dashboard page application.
        """
        super().show_main_content()
        alert_by_date = AlertByDate()
        df_arretes = alert_by_date.get_arretes_en_vigueur(self.side_bar_infos)
        if df_arretes.empty:
            st.warning("Les dates demander ne contiennent pas de donnée", icon="⚠️")
        else:
            alert_by_date.show_departement_by_level(
                df_arretes, self.side_bar_infos, self.style
            )
            if self.side_bar_infos.get("show_df"):
                st.subheader(
                    "Tableau général des données", divider=self.style.get("divider")
                )
                st.dataframe(df_arretes)
