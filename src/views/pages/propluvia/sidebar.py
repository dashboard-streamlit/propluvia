import streamlit as st

from src.models.get_data import GetData


def show_sidebar():
    """Show all widget for side bar."""
    with st.sidebar:
        if st.button("Téléchargement de toutes les données..."):
            with st.spinner("Téléchargement en cours"):
                get_data = GetData()
                get_data.get_data()
            st.success("Téléchargement terminé !", icon="✅")
        if st.button("Téléchargement des mises à jour..."):
            with st.spinner("Téléchargement en cours"):
                get_data = GetData()
                get_data.get_data(only_update=True)
            st.success("Téléchargement terminé !", icon="✅")
