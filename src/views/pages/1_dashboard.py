from src.views.pages.dashboard.main_content import DashboardMainContent
from src.views.tools.common import set_page_config
from src.views.tools.generic_sidebar import show_sidebar

set_page_config("Dashboard")
side_bar_infos = show_sidebar()
get_readme_path = ["src", "views", "pages", "dashboard"]
dashboard_main_content = DashboardMainContent(
    side_bar_infos=side_bar_infos, get_readme_path=get_readme_path
)
dashboard_main_content.show_main_content()
