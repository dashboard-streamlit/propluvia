from dataclasses import dataclass

import folium
import geopandas as gpd
import streamlit as st
from streamlit_folium import st_folium

from src.models.format_data import FormatData
from src.models.geojson import GeoJson
from src.views.tools.geojson_view import GeoJsonView
from src.views.tools.main_content import MainContent


@dataclass
class CarteMainContent(MainContent):
    """Class CarteMainContent for all widget on the carte main content."""

    @st.cache_data
    def get_geodataframe(self, name: str) -> gpd.GeoDataFrame:
        geo_json = GeoJson()
        columns = [
            "id_arrete",
            "nom_zone",
            "type_zone",
            "nom_departement",
            "debut_validite_arrete",
            "fin_validite_arrete",
            "nom_niveau",
            "geometry",
        ]
        choose_date = self.side_bar_infos.get("choose_date")
        start_date_col = "debut_validite_arrete"
        end_date_col = "fin_validite_arrete"
        filters = f"""
        WHERE {start_date_col}  <= '{choose_date}'
        AND {end_date_col}  >=  '{choose_date}'
        """
        gdf = geo_json.get_geojson(table=name, columns=columns, filters=filters)
        gdf = geo_json.convert_timestamps_to_string(gdf)
        return gdf

    def get_folium_map(self, name: str, gdf: gpd.GeoDataFrame) -> folium.Map:
        """
        Creates a Folium map showcasing a GeoJSON dataset.

        Args:
            name (str): The name of the GeoJSON dataset to visualize.

        Returns:
            folium.Map: The created Folium map object.
        """
        geo_json = GeoJson()
        fields_to_show = geo_json.get_fields_to_show(gdf)
        column_to_show = geo_json.get_column_to_show(name)

        geo_json_view = GeoJsonView()
        folium_map = geo_json_view.set_default_map()
        folium_map = geo_json_view.add_geojson_on_map(
            folium_map,
            gdf=gdf,
            name=name,
            column_to_show=column_to_show,
            fields_to_show=fields_to_show,
        )
        return folium_map

    def show_folium_map(self, name: str, gdf: gpd.GeoDataFrame):
        """
        Displays a Folium map for a GeoJSON dataset with interactive features.

        This method leverages Streamlit (`st`) to create an interactive map.

        Args:
            name (str): The name of the GeoJSON dataset to visualize.
        """
        folium_map = self.get_folium_map(name=name, gdf=gdf)
        st_folium(
            folium_map,
            use_container_width=True,
            center=st.session_state["default_map_center"],
            zoom=st.session_state["default_map_zoom_start"],
            returned_objects=[],
        )

    def show_main_content(self):
        """
        Displays the main content of the cartes page application.
        """
        super().show_main_content()
        map_name = "Arrêtés en vigueur"
        name = FormatData.clean_str_values(map_name)
        gdf = self.get_geodataframe(name)
        if gdf.empty:
            st.warning("Les dates demander ne contiennent pas de donnée", icon="⚠️")
        else:
            self.show_folium_map(name, gdf)
            if self.side_bar_infos.get("show_df"):
                st.subheader(
                    "Tableau général des données", divider=self.style.get("divider")
                )
                st.dataframe(gdf.drop(columns=["geometry"]))
