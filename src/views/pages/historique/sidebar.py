import streamlit as st


def show_sidebar():
    """Show all widget for side bar."""
    with st.sidebar:
        with st.form("history_sidebar_form"):
            show_df = st.checkbox("Voir les données brutes")

            submitted = st.form_submit_button("Validé")
            if submitted:
                res = {
                    "show_df": show_df,
                }
            else:
                res = {
                    "show_df": show_df,
                }
            return res
