import logging
from dataclasses import dataclass
from typing import List

import pandas as pd
import streamlit as st

from src.models.db_handler import PgHandler
from src.models.explore_data import ExploreData
from src.views.tools.common import get_readme
from src.views.tools.main_content import MainContent
from src.views.tools.plot_fig import PlotFig


@dataclass
class HistoryMainContent(MainContent):
    """Class HistoryMainContent for all widget on the history main content."""

    def show_main_content(self):
        """
        Displays the main content of the history page application.
        """
        get_readme(self.get_readme_path)
        history = History(
            side_bar_infos=self.side_bar_infos, get_readme_path=self.get_readme_path
        )
        history.plot_area_alert()


@dataclass
class History(HistoryMainContent):
    """
    Class History dedicated to retrieving and presenting historical data.

    Attributes:
        start_date_col (str): The name of the column containing the start date.
            (default: "debut_validite_arrete")
        end_date_col (str): The name of the column containing the end date.
            (default: "fin_validite_arrete")
        levels (List[str]): A list of defined alert levels used for
            processing and visualization.
            (default: ["Vigilance", "Alerte", "Alerte renforcée", "Crise"])
    """

    start_date_col = "debut_validite_arrete"
    end_date_col = "fin_validite_arrete"
    levels = ["Vigilance", "Alerte", "Alerte renforcée", "Crise"]

    @property
    def total_zone_alerte(self):
        total_zone_alerte_str = "total_zone_alerte"
        sql_query = f"""
        SELECT count("index")
            AS {total_zone_alerte_str}
        FROM zones_alerte
        """
        df = PgHandler.read_dataframe_to_postgres_query(sql_query)
        total_zone_alerte = df[total_zone_alerte_str].iloc[0]
        return total_zone_alerte

    @st.cache_data
    def get_arretes_by_level(self, nom_niveau) -> pd.DataFrame:
        """
        Retrieves data on regulations (arrêtés) associated with a specific level.

        This method retrieves data from a PostgreSQL database and filters
        regulations based on the provided 'nom_niveau' (level name).
        It assumes the presence of pre-defined attributes:

        - `self.start_date_col`: The column name representing the start validity
            date of regulations in the database table.
        - `self.end_date_col`: The column name representing the end validity date
            of regulations in the database table.

        The method constructs a SQL query that:

        1. Selects specific columns from the 'arretes' and 'zones_alerte' tables.
        2. Performs an inner join between the tables based on the 'id_zone' column.
        3. Filters data based on two criteria:
            - 'z.type_zone = 'SUP'`: Filters for zones of a specific type
                ('SUP', likely meaning something like 'supported').
            - 'a.nom_niveau = '{nom_niveau}'`: Filters regulations based on
                the provided 'nom_niveau' (level name).

        The retrieved data is then loaded into a pandas DataFrame and returned.

        Args:
            nom_niveau (str): The level name (nom_niveau) to
            filter regulations by.

        Returns:
            pd.DataFrame: A pandas DataFrame containing information on regulations
                associated with the specified level. The DataFrame includes columns
                like 'id_zone' (zone ID), 'code_departement' (department code),
                'nom_departement' (department name), etc.
        """
        sql_query = f"""
        SELECT
            a.{self.start_date_col}, a.{self.end_date_col},
            a.id_zone, a.nom_niveau, a.numero_niveau,
            z.code_departement, z.nom_departement, z.surface_departement,
            z.surface_zone, z.type_zone
        FROM arretes a
        INNER JOIN zones_alerte z ON a.id_zone = z.id_zone
        where z.type_zone = 'SUP' and a.nom_niveau = '{nom_niveau}'
        """
        df = PgHandler.read_dataframe_to_postgres_query(sql_query)
        return df

    @staticmethod
    def compute_df_surface_zone_total_by_date(df):
        """
        Calculates the total surface area of zones considering duplicates.

        This function calculates the total surface area represented in a
        DataFrame assumed to contain information about zones.

        The function returns the calculated total surface area as a float.

        Args:
            df (pd.DataFrame): The DataFrame containing zone information,
                including an "id_zone" column and a "surface_zone" column
                representing the surface area of each zone.

        Returns:
            float: The total surface area of all zones in the DataFrame,
            rounded to two decimal places.
        """
        df_dropped = df.drop_duplicates(subset="id_zone", keep="first")
        surface_zone_total = df_dropped["surface_zone"].sum()
        return surface_zone_total.round(2)

    def compute_nb_alert(self, df: pd.DataFrame) -> int:
        """
        Computes and returns the number of alerts based on a provided DataFrame.

        Args:
            df (pd.DataFrame): The DataFrame containing the data to analyze for alerts.

        Returns:
            int: The total number of alerts detected in the DataFrame.
        """

        return df.shape[0]

    @staticmethod
    def concat_and_plot_dfs(
        dfs: List[pd.DataFrame], title: str, xaxis_title: str, yaxis_title: str
    ):
        """
        Concatenates a list of DataFrames, generates a scatter plot using
        PlotFig, and displays it.

        Args:
            dfs (List[pd.DataFrame]): A list of pandas DataFrames to be concatenated.
            title (str): The title for the generated plot.
            xaxis_title (str): The title for the x-axis of the plot.
            yaxis_title (str): The title for the y-axis of the plot.
        """
        df = pd.concat(dfs)
        fig = PlotFig.plot_scatter_by_date_go(
            df,
            df.columns.to_list(),
            title=title,
            xaxis_title=xaxis_title,
            yaxis_title=yaxis_title,
        )
        st.plotly_chart(fig, use_container_width=True)

    def get_level(
        self,
        df_arretes: pd.DataFrame,
        level: str,
        dfs_area_total: List[pd.DataFrame],
        dfs_alert_duration: List[pd.DataFrame],
    ) -> List[pd.DataFrame]:
        """
        Calculates and extracts data for a specific level, primarily surface
        area and alert duration.

        Args:
            df_arretes (pd.DataFrame): DataFrame containing main data.
            level (str): The specific level to process.
            dfs_area_total (List[pd.DataFrame]): List to store DataFrames
                holding surface area data for each level.
            dfs_alert_duration (List[pd.DataFrame]): List to store DataFrames
                holding alert duration data for each level.

        Returns:
            List[pd.DataFrame]: A tuple containing two lists of DataFrames,
                representing surface area data and alert duration data
                for the given level.
        """
        logging.info(f"Get surface data for level : {level}")
        df_zone_total_by_date = ExploreData.values_by_dates(
            df_arretes.copy(),
            level,
            self.start_date_col,
            self.end_date_col,
            self.compute_df_surface_zone_total_by_date,
        )
        df_alert_duration = ExploreData.values_by_dates(
            df_arretes.copy(),
            level,
            self.start_date_col,
            self.end_date_col,
            self.compute_nb_alert,
        )
        if df_arretes.empty or df_alert_duration.empty:
            st.warning("La base semble ne pas contenir de donnée", icon="⚠️")
            st.info("Penser à les télécharger")
        else:
            dfs_area_total.append(df_zone_total_by_date)
            dfs_alert_duration.append(df_alert_duration)
            if self.side_bar_infos.get("show_df"):
                st.subheader(
                    f"Donné niveau {level}",
                    divider=self.style.get("divider"),
                )
                st.subheader(
                    "Tableau général des données",
                    divider=self.style.get("divider"),
                )
                st.dataframe(df_arretes)
        return dfs_area_total, dfs_alert_duration

    def plot_area_alert(self):
        """
        Generates plots visualizing surface area under
        vigilance and alert duration over time.

        - Checks for available data and displays a warning if empty.
        - Iterates through levels (presumably alert levels) and processes
            data for each level.
        - Creates plots for surface area and alert duration using
            self.concat_and_plot_dfs().
        """
        # Today's date as a string (adjust format as needed)
        if self.total_zone_alerte == 0:
            st.warning("La base semble ne pas contenir de données.", icon="⚠️")
            st.info("Penser à les télécharger")
        else:
            with st.spinner("Calcul en cours, veuillez patienter."):
                dfs_area_total = []
                dfs_alert_duration = []
                for level in self.levels:
                    df_arretes = self.get_arretes_by_level(level)

                    dfs_area_total, dfs_alert_duration = self.get_level(
                        df_arretes, level, dfs_area_total, dfs_alert_duration
                    )

                if dfs_area_total:
                    title_area_total = """
                    Surface du territoire en vigilance
                     (pour les eaux superficielles uniquement)
                    """
                    xaxis_title = "Date"
                    yaxis_title_area_total = "Surface en km²"
                    self.concat_and_plot_dfs(
                        dfs_area_total,
                        title=title_area_total,
                        xaxis_title=xaxis_title,
                        yaxis_title=yaxis_title_area_total,
                    )
                if dfs_alert_duration:
                    title_alert_duration = (
                        "Nombre d'arrêtés de restriction dans le temps"
                    )
                    yaxis_title_alert_duration = "Nombre d'arrêts simultanés"
                    self.concat_and_plot_dfs(
                        dfs_alert_duration,
                        title=title_alert_duration,
                        xaxis_title=xaxis_title,
                        yaxis_title=yaxis_title_alert_duration,
                    )
