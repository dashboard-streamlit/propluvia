import logging

import streamlit as st

from src.views.pages.cartes.main_content import CarteMainContent
from src.views.tools.common import set_page_config
from src.views.tools.generic_sidebar import show_sidebar

set_page_config("Cartes")
side_bar_infos = show_sidebar()
get_readme_path = ["src", "views", "pages", "cartes"]
map_stat = "side_bar_infos"
# if not st.session_state.get(map_stat) or st.session_state.get(map_stat) != side_bar_infos:
logging.info(f"Show map main content 🗺️ {side_bar_infos}")
carte_main_content = CarteMainContent(
    side_bar_infos=side_bar_infos, get_readme_path=get_readme_path
)
carte_main_content.show_main_content()
st.session_state[map_stat] = side_bar_infos
