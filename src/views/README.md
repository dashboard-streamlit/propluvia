# Donnée Sécheresse 🐪 - Propluvia 🌦️

## Description
La donnée manipulée concerne les restrictions liées à la sécheresse.
Ces données sont publiées sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/donnee-secheresse-propluvia/)

## Utulisation

1. ⚠️ Pour la première utilisation, cliquer 🖱️ sur le bouton `Téléchargement de toutes les données`

## Pages
