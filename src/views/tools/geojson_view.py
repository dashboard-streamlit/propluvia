from dataclasses import dataclass

import folium
import geopandas as gpd
import streamlit as st

from src.config import VIEW_CONFIG
from src.models.input_output import InputOuput
from src.views.tools.folium import Folium


@dataclass
class GeoJsonView(Folium):
    """Class GeoJsonView to manage view part of geojson."""

    def add_geojson_on_map(
        self,
        map: folium.Map,
        gdf: gpd.GeoDataFrame,
        name: str,
        column_to_show: str,
        fields_to_show: list,
    ) -> folium.Map:
        """
        Adds a GeoJSON layer to a folium map with styling and hover tooltips.

        This function takes a folium map object, a GeoDataFrame containing geospatial data, a name for the GeoJSON layer, the column to use for styling, and a list of columns to display in tooltips. It retrieves style codes from a YAML configuration file based on the specified column and creates styled GeoJSON layer with hover tooltips for each feature.

        Args:
            map (folium.Map): The folium map object to add the GeoJSON layer to.
            gdf (gpd.GeoDataFrame): The GeoDataFrame containing the geospatial data.
            name (str): The name to be displayed in the map legend for the GeoJSON layer.
            column_to_show (str): The name of the column in the GeoDataFrame to use for feature styling based on a style configuration file.
            fields_to_show (list): A list of column names to display in hover tooltips for each GeoJSON feature.

        Returns:
            folium.Map: The modified folium map object with the added GeoJSON layer.
        """
        style_codes = InputOuput.get_data_from_yaml(VIEW_CONFIG, column_to_show)
        tooltip = folium.GeoJsonTooltip(fields=fields_to_show)
        folium.GeoJson(
            data=gdf.to_json(),
            name=name,
            style_function=lambda f: self.style_function(
                f, column_to_show, style_codes
            ),
            tooltip=tooltip,
        ).add_to(map)
        return map

    @staticmethod
    def set_default_map(config_key: str = "default_map") -> folium.Map:
        """
        Creates a default folium map based on configuration in a YAML file.

        This static method retrieves map configuration (center coordinates and zoom level)
        from a YAML file using a specified key. It then uses these values to create a
        folium Map object.

        Args:
            config_key (str, optional): The key within the YAML file to use
            for retrieving default map configuration. Defaults to "default_map".

        Returns:
            folium.Map: A folium Map object initialized with default center
            coordinates and zoom level.
        """
        default_map = InputOuput.get_data_from_yaml(VIEW_CONFIG, config_key)
        st.session_state["default_map_center"] = [
            default_map["center"]["lat"],
            default_map["center"]["lon"],
        ]
        st.session_state["default_map_zoom_start"] = default_map["zoom_start"]
        return folium.Map(
            location=st.session_state["default_map_center"],
            zoom_start=st.session_state["default_map_zoom_start"],
        )
