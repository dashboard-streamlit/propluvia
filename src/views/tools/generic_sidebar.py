from datetime import date

import streamlit as st

from src.config import DATE_FMT


def show_sidebar():
    """Show all widget for side bar."""
    with st.sidebar:
        with st.form("dashboard_sidebar_form"):
            today = date.today()
            help = """
Date >= Au début de la validité de l'arrêt Date <= À la fin de la validité de l'arrêt
"""
            choose_date = st.date_input(
                "Date comprise dans les arrêts",
                today,
                format="YYYY-MM-DD",
                help=help,
            )
            choose_date = choose_date.strftime(DATE_FMT)
            show_df = st.checkbox("Voir les données brutes")

            submitted = st.form_submit_button("Validé")
            if submitted:
                res = {
                    "choose_date": choose_date,
                    "show_df": show_df,
                }
            else:
                res = {
                    "choose_date": today.strftime(DATE_FMT),
                    "show_df": show_df,
                }
            return res
