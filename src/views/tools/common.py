from pathlib import PurePath

import streamlit as st


@st.cache_data
def get_readme(readme_path: list[str]) -> None:
    """
    Fetches and displays the contents of a README.md file within a Streamlit app.

    This function retrieves the content of a README.md file located at the specified path and displays it as markdown within a Streamlit app. The path is provided as a list of strings representing the directory structure.

    Args:
        readme_path (list[str]): A list of strings representing the path to the README.md file.
            Each element represents a directory level.

    Returns:
        None: This function modifies the Streamlit app layout by displaying the markdown content, but doesn't return a value.

    Raises:
        IOError: If the specified README.md file cannot be found or opened.
    """
    try:
        with open(
            PurePath(*readme_path, "README.md"), "r", encoding="utf-8"
        ) as readme_file:
            readme_content = readme_file.read()
        st.markdown(readme_content)
    except IOError as e:
        # Handle file I/O exceptions gracefully (e.g., display an error message)
        st.error(f"Failed to load README.md: {e}")


def set_page_config(page_title: str):
    st.set_page_config(
        page_title=page_title,
        page_icon="🌦️",
        layout="wide",
        initial_sidebar_state="expanded",
        menu_items={
            "Get Help": "https://gitlab.com/dashboard-streamlit/propluvia/-/blob/main/README.md?ref_type=heads",
            "Report a bug": "https://gitlab.com/dashboard-streamlit/propluvia/-/issues",
            "About": "# [Source code](https://gitlab.com/dashboard-streamlit/propluvia)",
        },
    )
