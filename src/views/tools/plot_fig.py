from dataclasses import dataclass

import plotly.graph_objects as go


@dataclass
class PlotFig:
    """Class PlotFig regroup graph and plot figures tools."""

    @staticmethod
    def plot_scatter_by_date_go(
        df,
        y_cols,
        title,
        xaxis_title,
        yaxis_title,
        max_y: float = None,
        max_label: str = None,
    ):
        """
        Creates a multiple-line scatter plot with optional horizontal line.

        This static method generates a Plotly figure containing multiple lines
        plotted against a date index,
        with each line representing a different column from the DataFrame.
        It optionally includes a horizontal line to highlight a specific value.

        Args:
            df (pd.DataFrame): The DataFrame containing the data to plot.
                Its index should be dates.
            y_cols (List[str]): A list of column names from the DataFrame
                to plot as separate lines.
            title (str): The title of the plot.
            xaxis_title (str): The title for the x-axis.
            yaxis_title (str): The title for the y-axis.
            max_y (float, optional): If provided, a horizontal line with
                this y-value will be added. Defaults to None.
            max_label (str, optional): The name for the horizontal line (
                if max_y is provided). Defaults to None.

        Returns:
            go.Figure: The Plotly figure object containing the scatter plot.
        """
        # Get unique dates from the index
        index = df.index
        fig = go.Figure()

        # Create a horizontal line trace for the specific value
        if max_y:
            fig.add_trace(
                go.Scatter(
                    x=[index.min(), index.max()],
                    y=[max_y, max_y],  # Set y-axis
                    mode="lines",  # Set mode to lines only
                    line=dict(color="red", dash="dash"),
                    name=max_label,  # Optional name for the line
                )
            )

        for col in y_cols:
            # Create a scatter trace for each column
            fig.add_trace(
                go.Scatter(
                    x=df.index,
                    y=df[col],
                    mode="lines+markers",  # Set line and marker style
                    name=col,
                )
            )

        # Update layout (adjust as needed)
        fig.update_layout(title=title, xaxis_title=xaxis_title, yaxis_title=yaxis_title)

        return fig
