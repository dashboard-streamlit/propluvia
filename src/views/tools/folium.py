from dataclasses import dataclass


@dataclass
class Folium:
    """Class Folium to group folium tweaks"""

    def style_function(self, feature, column_to_search: str, style_code: dict) -> dict:
        """
        Applies custom styling to GeoJSON features based on a specified column and style dictionary.

        Args:
            feature: A GeoJSON feature object containing a "properties" dictionary.
            column_to_search (str): The name of the column within "properties" to use for styling.
            style_code (dict): A dictionary mapping feature values to corresponding style dictionaries.

        Returns:
            dict: A style dictionary to be applied to the given feature.

        Raises:
            KeyError: If the feature value doesn't match any key in the style_code dictionary.
        """

        feature_value = feature["properties"][column_to_search]

        for key, value in style_code.items():
            if feature_value == key:
                return value

        # If no match is found, raise a KeyError to indicate an invalid feature value
        raise KeyError(f"Invalid feature value '{feature_value}' for styling")
