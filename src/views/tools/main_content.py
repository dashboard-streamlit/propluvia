from dataclasses import dataclass

import streamlit as st

from src.config import VIEW_CONFIG
from src.models.input_output import InputOuput
from src.views.tools.common import get_readme


@dataclass
class MainContent:
    """Class MainContent for all widget on the dashboard main content."""

    side_bar_infos: dict
    get_readme_path: list
    style = InputOuput.get_data_from_yaml(VIEW_CONFIG, "style")

    def show_main_content(self):
        """
        Displays the main content of the dashboard page application.
        """
        choose_date = self.side_bar_infos.get("choose_date")
        get_readme(self.get_readme_path)
        chart_name = f"Arrêtés en vigueur le {choose_date}"
        st.subheader(chart_name, divider=self.style.get("divider"))
