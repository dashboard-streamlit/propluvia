import logging
from pathlib import Path, PurePath

LOGGING_LEVEL = logging.INFO
MODEL_CONFIG = PurePath("src", "models", "config.yaml")
DL_DEFAULT = "resources_to_download_one_shot"
DL_UPDATE = "resources_to_download_to_update"
VIEW_CONFIG = PurePath("src", "views", "config.yaml")
DL_DATA_DIR = Path("dl_data")
DATE_FMT = "%Y-%m-%d"
SECOND_DATE_FMT = "00%y-%m-%d"
THIRD_DATE_FMT = "%Y-%m-%d %H:%M:%S"
