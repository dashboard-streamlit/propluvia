from dataclasses import dataclass

import geopandas as gpd
import pandas as pd
import streamlit as st

from src.config import DATE_FMT, DL_DEFAULT, DL_UPDATE, MODEL_CONFIG
from src.models.db_handler import PgHandler
from src.models.format_data import FormatData
from src.models.input_output import InputOuput


@dataclass
class GeoJson:
    """Class with GeoJson tools."""

    @st.cache_data
    def get_geojson(
        self, table: str, columns: list = None, filters: str = None
    ) -> gpd.GeoDataFrame:
        """Loads and prepares GeoJSON data from a PostgreSQL table.

        This function retrieves GeoJSON data from a table in a PostgreSQL
        database using PgHandler. It then cleans string column values
        (assuming a specific column) using the FormatData class.

        Args:
            table (str): The name of the table in the PostgreSQL
                database containing GeoJSON data.
            columns (list, optional): A list of column names to select.
                Defaults to None (select all columns).
            filters (str, optional): A additional filter like 'WHERE ...'.
                Defaults to None (select all columns).

        Returns:
            gpd.GeoDataFrame: The loaded and cleaned GeoDataFrame object.
        """
        gdf = PgHandler.read_geojson_from_postgis(
            table=table, columns=columns, filters=filters
        )
        column_to_show = self.get_column_to_show(name=table)
        gdf = FormatData.clean_column_str_values(gdf, column_to_show)
        return gdf

    @staticmethod
    def convert_timestamps_to_string(gdf):
        """Converts all timestamp columns in a GeoDataFrame to strings.

        Args:
            gdf (gpd.GeoDataFrame): The GeoDataFrame to modify.

        Returns:
            gpd.GeoDataFrame: The modified GeoDataFrame with timestamp columns converted to strings.
        """

        # Get all column names
        all_cols = list(gdf.columns)

        # Select timestamp columns using list comprehension
        timestamp_cols = [
            col for col in all_cols if pd.api.types.is_datetime64_dtype(gdf[col])
        ]

        # Convert timestamps to strings with desired format
        for col in timestamp_cols:
            gdf[col] = gdf[col].dt.strftime(DATE_FMT)

        return gdf

    @staticmethod
    def get_fields_to_show(gdf: gpd.GeoDataFrame) -> list:
        """
        Extracts a list of non-geometry columns from a GeoDataFrame.

        This static method takes a GeoDataFrame as input and returns a list
        containing all column names except the "geometry" column.

        Args:
            gdf (gpd.GeoDataFrame): The GeoDataFrame object containing the data.

        Returns:
            list: A list of column names (excluding "geometry") suitable for
                displaying in tooltips or other contexts.
        """
        fields_to_show = gdf.columns.to_list()
        fields_to_show.remove("geometry")
        return fields_to_show

    @st.cache_data
    def get_column_to_show(self, name: str) -> str:
        """
        Retrieves the column name to be used for styling or display from a YAML configuration file.

        This function takes a resource name (presumably representing a GeoJSON layer) as input. It retrieves the corresponding resource configuration from a YAML file using InputOuput. It then extracts the "column_to_show" value associated with the resource. If the resource configuration is missing or the "column_to_show" key is absent, a ValueError is raised.

        Args:
            name (str): The name of the resource (GeoJSON layer) to retrieve the column information for.

        Returns:
            str: The column name to be used for styling or display based on the resource configuration.

        Raises:
            ValueError: If the resource configuration is missing from the YAML file or the "column_to_show" key is not found within the configuration.
        """
        resources = InputOuput.get_data_from_yaml(
            file_path=MODEL_CONFIG, key_to_get=DL_UPDATE
        )
        resources.update(
            InputOuput.get_data_from_yaml(file_path=MODEL_CONFIG, key_to_get=DL_DEFAULT)
        )
        resource = resources.get(name)
        column_to_show = resource.get("column_to_show")
        if not column_to_show:
            raise ValueError(f"Any {name} in resources 'name' or any 'column_to_show'")
        return column_to_show
