import logging
from dataclasses import dataclass
from os import getenv
from pathlib import Path

import geopandas as gpd
import pandas as pd
import streamlit as st
from dotenv import load_dotenv
from sqlalchemy import Engine, create_engine, text


@dataclass
class PgHandler:
    """Class PgHandler to manage interaction with postgres database."""

    @st.cache_resource
    @staticmethod
    def get_postgresql_engine(env_path: Path = Path(".env")) -> Engine:
        """
        Creates and returns a SQLAlchemy engine object for connecting to a PostgreSQL database.

        This function retrieves database credentials (dbname, host, port, user, password) from environment variables
        prefixed with 'POSTGRES_' using the `load_dotenv` function. It then constructs a connection URL and uses
        `create_engine` from SQLAlchemy to establish a connection to the specified database.


        **Important:**

        - Ensure you have the `psycopg2` library installed, which is a prerequisite for connecting to PostgreSQL with SQLAlchemy.
        - Make sure the required environment variables (POSTGRES_DB, POSTGRES_HOST, POSTGRES_PORT, POSTGRES_USER, POSTGRES_PASSWORD)
        are set with the appropriate connection details for your PostgreSQL database.

        Args:
            env_path (Path): Path to the .env file with the environment variables.
                Defaults to Path(".env").

        Returns:
            create_engine: A SQLAlchemy engine object for interacting with the PostgreSQL database.
        """
        if Path(env_path).is_file:
            env_set = load_dotenv(env_path, override=True)
        envs_to_check = [
            "POSTGRES_DB",
            "POSTGRES_PORT",
            "POSTGRES_USER",
            "POSTGRES_PASSWORD",
        ]
        for env in envs_to_check:
            if not getenv(env):
                raise EnvironmentError(f"Env {env} not set")
            env_set = True

        if not env_set:
            raise EnvironmentError(f"Any environment files find in {env_path}")
        dbname = getenv("POSTGRES_DB")
        host = getenv("POSTGRES_HOST")
        port = getenv("POSTGRES_PORT")
        user = getenv("POSTGRES_USER")
        password = getenv("POSTGRES_PASSWORD")

        # Connect to an existing database
        engine = create_engine(f"postgresql://{user}:{password}@{host}:{port}/{dbname}")
        logging.debug("Get postgresql engine from POSTGRES_* env")
        return engine

    @staticmethod
    def select_sql_query_simple(
        table: str, columns: list = None, filters: str = None
    ) -> str:
        """
        Generates a simple SELECT SQL query string for a given table.

        This function constructs a basic SELECT query that retrieves data from a specified table.

        Args:
            table (str): The name of the table to query.
            columns (list, optional): A list of column names to select.
                Defaults to None (select all columns).
            filters (str, optional): A additional filter like 'WHERE ...'.
                Defaults to None (select all columns).

        Returns:
            str: The generated SELECT SQL query string.

        Examples:
            >>> select_sql_query_simple("customers")
            'SELECT * FROM customers'

            >>> select_sql_query_simple("orders", ["id", "product_name", "quantity"])
            'SELECT id, product_name, quantity FROM orders'
        """
        if not columns:
            columns_str = "*"
        else:
            # Use ", ".join() for proper comma separation
            columns_str = ", ".join(columns)
        if not filters:
            filters = ""
        query = f"SELECT {columns_str} FROM {table} {filters}"
        logging.info(query)
        return query

    @staticmethod
    def save_geojson_to_postgis(
        geo_data_frame: gpd.GeoDataFrame, table: str, if_exists="replace"
    ):
        """
        Saves a GeoDataFrame containing GeoJSON data to a PostGIS database table.

        This function leverages GeoPandas' `to_postgis` method to efficiently write a GeoDataFrame to a PostGIS table.

        Args:
            geo_data_frame (gpd.GeoDataFrame): The GeoDataFrame containing GeoJSON data to be saved.
            table (str): The name of the PostGIS table to save the data into.
            if_exists (str, optional): Specifies behavior if the table already exists.
                Defaults to 'replace', which drops and recreates the table. Other options include
                'append', 'fail', and 'delete'. See GeoPandas documentation for details.

        Raises:
            ValueError: If an invalid value is provided for `if_exists`.

        Logs:
            Logs a message indicating the table name and `if_exists` behavior for tracking purposes.

        Examples:
            >>> gdf = gpd.read_file("path/to/geojson.json")
            >>> save_geojson_to_postgis(gdf, "my_postgis_table")  # Replace table if exists
            >>> save_geojson_to_postgis(gdf, "other_table", if_exists="append")  # Append to existing table
        """
        engine = PgHandler.get_postgresql_engine()

        geo_data_frame.to_postgis(table, engine, if_exists=if_exists)
        logging.info(f"Save GeoDataFrame to {table} with rule if_exists: {if_exists}")

    @staticmethod
    def read_geojson_from_postgis(
        table: str,
        columns: list = None,
        geom_col: str = "geometry",
        filters: str = None,
    ) -> gpd.GeoDataFrame:
        """
        Reads GeoJSON data from a PostGIS database table into a GeoDataFrame.

        This function retrieves geospatial data from a specified table in a
        PostGIS database and constructs a GeoDataFrame object using GeoPandas.
        It allows you to optionally specify which column contains the
        GeoJSON data geometry.

        Args:
            table (str): The name of the PostGIS table containing the GeoJSON data.
            columns (list, optional): A list of column names to select.
                Defaults to None (select all columns).
            geom_col (str, optional): The name of the column containing
                the GeoJSON geometry data. Defaults to "geometry" (common case).
            filters (str, optional): A additional filter like 'WHERE ...'.
                Defaults to None (select all columns).

        Returns:
            gpd.GeoDataFrame: The GeoDataFrame containing the loaded geospatial data.

        Raises:
            ValueError: If an invalid value is provided for `geom_col`.

        Logs:
            Logs a message indicating the table name from which data is being read.

        Examples:
            >>> df = read_geojson_from_postgis("public.my_geodata")

            >>> # Read specific columns and geometry column named 'shape'
            >>> df = read_geojson_from_postgis("regions", ["region_name", "shape"], geom_col="shape")
        """
        engine = PgHandler.get_postgresql_engine()
        sql_query = PgHandler.select_sql_query_simple(table, columns, filters)
        df = gpd.read_postgis(sql_query, engine, geom_col=geom_col)
        logging.info(f"read from {table}")
        return df

    @staticmethod
    def save_dataframe_to_postgres(df: pd.DataFrame, table: str, if_exists="replace"):
        """
        Saves a pandas DataFrame to a PostgreSQL table.

        This function establishes a connection to a PostgreSQL database engine
        (obtained through PgHandler) and uses it to save the provided DataFrame
        to a specified table. It allows control over how existing tables are
        handled with the `if_exists` parameter.

        Args:
            df (pd.DataFrame): The pandas DataFrame to be saved.
            table (str): The name of the table to create or update in the PostgreSQL database.
            if_exists (str, optional): How to handle existing tables with the same name.
                Defaults to "replace", which drops the existing table if it exists.
                Other options include "append" and "fail".
        """
        engine = PgHandler.get_postgresql_engine()
        with engine.begin() as connection:
            df.to_sql(name=table, con=connection, if_exists=if_exists)
            logging.info(
                f"Save DataFrame to {table} with rule if_exists: {if_exists} 💾"
            )

    @staticmethod
    def read_dataframe_to_postgres_table(table: str) -> pd.DataFrame:
        """
        Reads a DataFrame from a PostgreSQL table.

        This static method retrieves a pandas DataFrame from a specified
        table in a PostgreSQL database

        Args:
            table (str): The name of the table to read data from in the
                PostgreSQL database.

        Returns:
            pd.DataFrame: The DataFrame containing data from the specified table.
        """
        engine = PgHandler.get_postgresql_engine()
        with engine.begin() as connection:
            df = pd.read_sql_table(table, connection)
            logging.info(f"Retrieve DataFrame from {table}")
        return df

    @staticmethod
    def read_dataframe_to_postgres_query(query: str) -> pd.DataFrame:
        """
        Reads a DataFrame from a PostgreSQL data.

        This static method retrieves a pandas DataFrame from a specified
        query in a PostgreSQL database

        Args:
            query (str): The sql query read data from in the PostgreSQL database.

        Returns:
            pd.DataFrame: The DataFrame containing data from the specified table.
        """
        engine = PgHandler.get_postgresql_engine()
        with engine.begin() as connection:
            df = pd.read_sql_query(query, connection)
            logging.info(f"Retrieve DataFrame from {query}")
        return df

    @staticmethod
    def execute_query(query: str):
        """Executes a provided SQL query string against a PostgreSQL database.

        Args:
            query (str): The SQL query string to be executed.

        **Important Note:**

        This approach directly executes the query string. While convenient, it's
        generally recommended to use SQLAlchemy's parameterization for queries to
        prevent SQL injection vulnerabilities. Consider using a method like
        `connection.execute(query, params)` with separate parameters for improved
        security.
        """
        engine = PgHandler.get_postgresql_engine()
        logging.info(query)
        with engine.begin() as connection:
            _ = connection.execute(text(query))
