from dataclasses import dataclass
from datetime import timedelta

import pandas as pd


@dataclass
class ExploreData:
    """Class ExploreData to make data transformation or exploration"""

    @staticmethod
    def get_index_value(df: pd.DataFrame, key_to_find: str) -> int:
        """
        Retrieves the value from a DataFrame at a specific index if it exists,
        otherwise returns 0.

        This function searches for the provided 'key_to_find' within the
        DataFrame's index. The index refers to the labels used to access rows
        in the DataFrame.

        If the key is found in the index:
            - The function retrieves the entire row (all columns) associated
            with that index using `.loc` and returns it as a pandas Series.

        If the key is not found in the index:
            - The function returns the default value of 0.

        Args:
            df (pd.DataFrame): The DataFrame to search for the index.
            key_to_find (str): The key (index label) to search for.

        Returns:
            int or pd.Series:
                - If the key is found in the index, returns the entire row
                  (all columns) as a pandas Series.
                - If the key is not found, returns the default value of 0 (integer).
        """
        if key_to_find in df.index.values:
            value = df.loc[key_to_find].values
        else:
            value = 0
        return value

    @staticmethod
    def values_by_dates(
        df, compute_result: str, start_date_col: str, end_date_col: str, apply_func
    ):
        """
        Calculates values based on a user-defined function for each date
        within a date range in a DataFrame.

        This function iterates through dates in a DataFrame and calculates a
        series of values based on a user-defined function (`apply_func`).
        The function operates on data points within a date range specified
        by the `start_date_col` and `end_date_col` columns.

        Args:
            df (pd.DataFrame): The DataFrame containing the data for calculation.
            compute_result (str): The name to use for the column in the
                output DataFrame that will store the calculated values.
            start_date_col (str): The name of the column containing
                the start date for each data point.
            end_date_col (str): The name of the column containing
                the end date for each data point.
            apply_func (Callable): A user-defined function that takes a filtered
                DataFrame subset based on a specific date and returns a single
                value representing the calculation for that date.

        Returns:
            pd.DataFrame: A DataFrame with a datetime index named "dates"
                and a column named after the provided `compute_result`
                containing the calculated values for each date.
        """
        start_date_obj = df[start_date_col].min()
        end_date_obj = df[end_date_col].max()

        data_computed = []
        df_dates = []

        # Loop through each date using a loop with timedelta
        current_date = start_date_obj
        while current_date <= end_date_obj:
            # Filter based on exact date match and range check
            df_mask = df[
                (current_date >= df[start_date_col])
                & (current_date <= df[end_date_col])
            ]

            data_computed.append(apply_func(df_mask))
            df_dates.append(current_date)
            current_date += timedelta(days=1)  # Move to the next day
        dates_col = "dates"
        data = {
            dates_col: df_dates,
            compute_result: data_computed,
        }
        df_surface_zone_total_by_date = pd.DataFrame(data).set_index(dates_col)
        return df_surface_zone_total_by_date
