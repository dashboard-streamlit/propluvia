import gzip
import logging
import shutil
from dataclasses import dataclass
from pathlib import Path

import yaml


@dataclass
class InputOuput:
    """Class InputOuput tools to read and with local data."""

    @staticmethod
    def compress_to_gz(file_to_compress: Path) -> Path:
        """
        Compresses a file into a .gz archive.

        Args:
            file_to_compress: The PosixPath object representing the file to compress.

        Returns:
            A PosixPath object representing the compressed .gz archive.

        Raises:
            ValueError: If the provided path doesn't point to a file.
        """

        if not file_to_compress.is_file():
            raise ValueError("Provided path does not point to a file")

        # Construct the output filename with .gz extension
        compressed_filename = f"{file_to_compress.name}.gz"
        compressed_path = file_to_compress.with_name(compressed_filename)

        # Open the files in appropriate modes
        with gzip.open(compressed_path, "wb") as compressed_file:
            with open(file_to_compress, "rb") as f_in:
                shutil.copyfileobj(f_in, compressed_file)

        return compressed_path

    @staticmethod
    def unzip_gz(gz_file: Path) -> Path:
        """
        Unzips a file with the .gz extension from a PosixPath object.

        Args:
            gz_file: The PosixPath object representing the .gz file to unzip.

        Returns:
            A PosixPath object representing the unzipped file.

        Raises:
            ValueError: If the provided path doesn't point to a file or the file extension is not .gz.
        """
        if not Path(gz_file).is_file():
            raise ValueError("Provided path does not point to a file")

        if gz_file.suffix != ".gz":
            raise ValueError("Provided file is not a .gz archive")

        # Extract filename without the .gz extension
        unzipped_filename = gz_file.stem

        # Create the output path for the unzipped file in the same directory
        unzipped_path = gz_file.with_name(unzipped_filename)

        # Perform the actual unzipping
        with gzip.open(gz_file, "rb") as f_in:
            with open(unzipped_path, "wb") as f_out:
                shutil.copyfileobj(f_in, f_out)

        return unzipped_path

    @staticmethod
    def get_data_from_yaml(file_path: Path, key_to_get: str = None):
        """
        This function reads a YAML file and returns the value associated with the
        provided key.

        Args:
            file_path (Path): Path to the YAML file.
            key_to_get (str, optional): Specific key to retrieve data from.
                Defaults to None, returning the entire data dictionary.

        Returns:
            Any: The value associated with the key (list, dict, str etc.) or
                the entire data dictionary if no key is provided.
        """

        try:
            with open(str(file_path), "r") as stream:
                data = yaml.safe_load(stream)
            if key_to_get:
                return data.get(key_to_get)
            else:
                return data
        except FileNotFoundError:
            logging.error(f"Error: YAML file not found at {file_path}")
            return None
        except yaml.YAMLError as exc:
            logging.error(f"Error parsing YAML file: {exc}")
            return None
