import logging
from dataclasses import dataclass
from os import listdir, makedirs, path, remove
from pathlib import Path, PurePath

import geopandas as gpd
import pandas as pd
import requests_cache
from retry_requests import retry

from src.config import DL_DATA_DIR, DL_DEFAULT, DL_UPDATE, MODEL_CONFIG
from src.models.db_handler import PgHandler
from src.models.format_data import FormatData
from src.models.input_output import InputOuput


@dataclass
class GetData:
    """Class GetData to retrieve the data."""

    @staticmethod
    def download_resource(
        resource_name: str,
        url: str,
        cache_dir="cache",
        retry_count=3,
        expire_after=-1,
    ):
        """
        This function downloads resource from a dictionary containing
        'name' and 'url' keys. It uses caching and retries for robustness.

        Args:
            resource_name (str): Name of the resource.
            url (url): Url to download the resource.
            cache_dir (str, optional): Directory to store cached responses. Defaults to "cache".
            retry_count (int, optional): Number of retry attempts on download failures. Defaults to 3.
            expire_after (int, optional): Parameter to set a fixed expiration time for all new responses. Defaults to -1.
        """
        session = requests_cache.CachedSession(
            cache_dir=cache_dir, expire_after=expire_after
        )
        retry_session = retry(session, retries=retry_count)
        logging.info(f"download from {resource_name}")
        req = retry_session.get(url)
        req.raise_for_status()
        return req

    def download_resources(
        self,
        config_file_path: Path,
        key_to_get: str,
        is_update: bool = True,
        dl_data_dir: Path = DL_DATA_DIR,
    ):
        """
        Downloads resources specified in a YAML configuration file.

        Args:
            config_file_path (Path): Path to the YAML configuration file
                containing resource information.
            key_to_get (str): The key within the YAML file that specifies
                the resources to download.
            is_update (bool): To now update case or starting case.
            dl_data_dir (Path, optional): Path to the directory where downloaded
                resources will be saved. Defaults to DL_DATA_DIR (defined elsewhere).

        Downloads resources based on the following steps:

        1. Creates the download directory (`dl_data_dir`) if it doesn't exist,
            handling potential existing directories using `exist_ok=True`.
        2. Reads data from the YAML configuration file using
            `InputOuput.get_data_from_yaml`.
        3. Iterates over each resource in the retrieved data:
            - Extracts URLs from the resource dictionary (keys starting with "url").
            - Downloads each resource URL using `GetData.download_resource`.
            - Constructs the target filename for the downloaded resource by
                combining the resource name and extension from the resource data.
            - Saves the downloaded content to the target file path within `dl_data_dir`.
            - Logs a message indicating successful download.
        4. Calls `self.uncompress_resources` (presumably to handle decompression
            of downloaded files) for the download directory.
        5. Calls `self.save_file_data_to_db` (presumably to store downloaded
            data in a database) for the download directory.

        This function assumes the presence of helper functions:
            - InputOuput.get_data_from_yaml
            - GetData.download_resource
            - self.uncompress_resources (likely defined within the same class)
            - self.save_file_data_to_db (likely defined within the same class)

        """

        # Create directory with 'exist_ok=True' to handle existing folder
        makedirs(dl_data_dir, exist_ok=True)

        resources_to_download = InputOuput.get_data_from_yaml(
            file_path=config_file_path, key_to_get=key_to_get
        )
        for resource_name, resource in resources_to_download.items():
            urls = [key for key in resource.keys() if str(key).startswith("url")]
            for url in urls:
                downloaded_file = GetData.download_resource(
                    resource_name, resource.get(url)
                )
                # Save downloaded file with name from resource inside 'dl_data' folder
                resource_file = f"{resource_name}{resource['ext']}"
                with open(path.join(dl_data_dir, resource_file), "wb") as f:
                    f.write(downloaded_file.content)
                logging.info(f"Downloaded {resource_name} {url} successfully 📥")
                self.uncompress_resources(dl_data_dir=dl_data_dir)
                self.save_file_data_to_db(dl_data_dir=dl_data_dir, is_update=is_update)

    @staticmethod
    def uncompress_resources(dl_data_dir: Path = DL_DATA_DIR):
        """
        Uncompresses all gzip (.gz) files within a data directory.

        This function iterates through the files in the specified data directory (defaulting to DL_DATA_DIR) and identifies files with the .gz extension. For each identified file, it performs the following:

        * Logs a message indicating the file being uncompressed.
        * Uses the InputOuput.unzip_gz function to decompress the file.
        * Removes the original compressed file (.gz).
        * Logs a message indicating the creation of the uncompressed file.

        Args:
            dl_data_dir (Path, optional): The directory path containing the compressed data files. Defaults to DL_DATA_DIR.

        """
        data_files = listdir(dl_data_dir)
        for data_file in data_files:
            if Path(data_file).suffix == ".gz":
                gz_file = PurePath(DL_DATA_DIR, data_file)
                logging.info(f"Unzip {gz_file} 🗜️")
                unzip__file = InputOuput.unzip_gz(gz_file)
                remove(gz_file)
                logging.info(f"{unzip__file} created")

    @staticmethod
    def query_remove_arrete_duplicate() -> str:
        query = """DELETE
        FROM arretes a1
        WHERE EXISTS (
            SELECT 1
            FROM arretes a2
            WHERE a1.id_arrete = a2.id_arrete
            AND a1.index <> a2.index
            ORDER BY a1.index
            LIMIT 1
        );
        """
        return query

    @staticmethod
    def save_file_data_to_db(dl_data_dir: Path = DL_DATA_DIR, is_update: bool = True):
        """
        Saves data from CSV and GeoJSON files in a directory to a PostgreSQL database.

        This function iterates through files in the specified data directory
        (defaulting to DL_DATA_DIR) and identifies CSV and GeoJSON files.
        It reads the data from the files using appropriate methods and saves it
        to corresponding tables in the PostgreSQL database using PgHandler.

        Key steps include:

        1. Load resource configuration from a YAML file to obtain table names and CSV separators.
        2. Iterate through files in the directory.
        3. Extract the table name from the file name by removing the file extension.
        4. Retrieve resource configuration for the table (if available).
        5. Load and save data based on file type:
        - For CSV files:
            - Load data using pandas.read_csv with the specified separator (defaulting to ",").
            - Save data to PostgreSQL using PgHandler.save_dataframe_to_postgres.
        - For GeoJSON files:
            - Load data using geopandas.read_file.
            - Save data to PostGIS (PostgreSQL with spatial extensions) using
                PgHandler.save_geojson_to_postgis.

        Args:
            dl_data_dir (Path, optional): The directory path containing the
                data files. Defaults to DL_DATA_DIR.
            is_update (bool): To now update case or starting case.
        """
        data_files = listdir(dl_data_dir)
        resources = InputOuput.get_data_from_yaml(
            file_path=MODEL_CONFIG, key_to_get=DL_UPDATE
        )
        resources.update(
            InputOuput.get_data_from_yaml(file_path=MODEL_CONFIG, key_to_get=DL_DEFAULT)
        )
        for data_file in data_files:
            logging.info(f"Save {data_file} to DB")
            data_path = PurePath(DL_DATA_DIR, data_file)
            table = Path(data_file).name.removesuffix(Path(data_file).suffix)
            resource = resources.get(table)
            format_data = FormatData()
            if_exists = resource.get("if_exists", "replace")
            if Path(data_file).suffix == ".csv":
                df = pd.read_csv(data_path, sep=resource.get("sep", ","))
                cols_to_drop = ["chemin_fichier", "chemin_fichier_arrete_cadre"]
                columns_to_drop = list(set(cols_to_drop) & set(df.columns.to_list()))
                df = df.drop(columns=columns_to_drop)
                df = format_data.format_date_columns(df, before_db=True)
                PgHandler.save_dataframe_to_postgres(df, table, if_exists=if_exists)
            elif Path(data_file).suffix == ".geojson":
                gdf = gpd.read_file(str(data_path))
                gdf = format_data.format_date_columns(gdf, before_db=True)
                PgHandler.save_geojson_to_postgis(gdf, table)
            else:
                raise NotImplementedError(f"{Path(data_file).suffix} not in use case")
            logging.info(f"Remove {data_path} 🗑️")
            remove(data_path)
            if is_update:
                logging.info("Clean DB 🧹")
                PgHandler.execute_query(GetData().query_remove_arrete_duplicate())

    def get_data(self, only_update: bool = False):
        """
        Downloads, uncompresses, and saves data files for processing.

        This method orchestrates the process of acquiring necessary data for
        further analysis.
        """
        logging.info("Start get data 📩")
        dl_data_dir = DL_DATA_DIR
        if not only_update:
            logging.info("Global dl")
            self.download_resources(
                config_file_path=MODEL_CONFIG,
                key_to_get=DL_UPDATE,
                dl_data_dir=dl_data_dir,
                is_update=False,
            )
            self.download_resources(
                config_file_path=MODEL_CONFIG,
                key_to_get=DL_DEFAULT,
                dl_data_dir=dl_data_dir,
                is_update=False,
            )
        else:
            self.download_resources(
                config_file_path=MODEL_CONFIG,
                key_to_get=DL_UPDATE,
                dl_data_dir=dl_data_dir,
                is_update=True,
            )
