import logging
from dataclasses import dataclass
from pathlib import Path

import pandas as pd

from src.config import DATE_FMT, MODEL_CONFIG, SECOND_DATE_FMT, THIRD_DATE_FMT
from src.models.check_data import CheckData
from src.models.input_output import InputOuput


@dataclass
class FormatData:
    """Class FormatData to format the data."""

    @staticmethod
    def clean_column_str_values(
        df: pd.DataFrame,
        column: str,
        model_config_file_path=MODEL_CONFIG,
    ) -> pd.DataFrame:
        """Cleans text values in a specified column of a DataFrame according
        to string replacement rules defined in a configuration file.

        Args:
            df (pd.DataFrame): The DataFrame containing the column to be cleaned.
            column (str): The name of the column to clean.
            model_config_file_path (_type_, optional): Path to a YAML
                configuration file containing replacement rules.
                Defaults to MODEL_CONFIG.

        Returns:
            pd.DataFrame:  The DataFrame with the cleaned column values.
        """
        df[column] = df[column].str.lower()
        str_to_replace = InputOuput.get_data_from_yaml(
            model_config_file_path, "str_to_replace"
        )
        for replace_rule in str_to_replace:
            df[column] = df[column].str.replace(
                replace_rule.get("old"), replace_rule.get("new")
            )
        return df

    @staticmethod
    def clean_str_values(
        str_to_change: str, model_config_file_path: Path = MODEL_CONFIG
    ) -> str:
        """
        Cleans a string by applying replacement rules from a configuration file.

        This static method performs string cleaning based on a configuration
        defined in a YAML file.

        Args:
            str_to_change (str): The string to be cleaned.
            model_config_file_path (Path, optional): The path to the YAML
                configuration file containing replacement rules.
                Defaults to MODEL_CONFIG.

        Returns:
            str: The cleaned string after applying replacements
                based on the configuration.
        """
        str_to_replace = InputOuput.get_data_from_yaml(
            model_config_file_path, "str_to_replace"
        )
        str_to_change = str_to_change.lower()
        for replace_rule in str_to_replace:
            str_to_change = str_to_change.replace(
                replace_rule.get("old"), replace_rule.get("new")
            )
        return str_to_change

    @staticmethod
    def swap_start_date_end_date(
        df: pd.DataFrame, date_start_col: str, date_end_col: str
    ) -> pd.DataFrame:
        """
        Swaps the names of two date columns in a pandas DataFrame.

        This static method renames the specified 'date_start_col' to 'date_end_col'
        and vice versa, effectively swapping their names.
        It does not modify the DataFrame's data, only the column labels.

        Args:
            df (pd.DataFrame): The DataFrame containing the date columns to be swapped.
            date_start_col (str): The name of the column currently containing start date values.
            date_end_col (str): The name of the column currently containing end date values.

        Returns:
            pd.DataFrame: A new DataFrame with the swapped column names.
        """
        logging.info("Swap invalid date column 🔃")
        new_col_names = {date_start_col: date_end_col, date_end_col: date_start_col}
        df = df.rename(columns=new_col_names)
        return df

    def format_date_columns(
        self, df: pd.DataFrame, before_db: bool = False
    ) -> pd.DataFrame:
        """
        Formats date columns in a pandas DataFrame based on configuration.

        This static method formats specific columns in a DataFrame as datetime
        objects.

        Args:
            df (pd.DataFrame): The pandas DataFrame to be processed.

        Returns:
            pd.DataFrame: The DataFrame with formatted date columns (if applicable).
        """
        date_columns = InputOuput.get_data_from_yaml(MODEL_CONFIG, "date_columns")
        start_date_col = None
        end_date_col = None
        for date_column, date_column_value in date_columns.items():
            if date_column in df.columns:
                logging.info(f"Format to date column {date_column} 📆")
                if before_db:
                    df = self.handle_date_parsing(df, date_column)
                else:
                    df[date_column] = pd.to_datetime(df[date_column])
                if before_db:
                    if not start_date_col and date_column_value.get("is_start_time"):
                        start_date_col = date_column
                    if not end_date_col and date_column_value.get("is_end_time"):
                        end_date_col = date_column
        if before_db:
            if start_date_col and end_date_col:
                invalid_date_df, valid_date_df = CheckData.start_date_upper_end_date(
                    df, start_date_col, end_date_col
                )
                invalid_date_swap = self.swap_start_date_end_date(
                    invalid_date_df, start_date_col, end_date_col
                )
                df = pd.concat([invalid_date_swap, valid_date_df])
        return df

    @staticmethod
    def check_date_format(date_str_to_test: str, formats: list) -> pd.Timestamp:
        """
        Checks if a date string in a DataFrame cell matches any of the specified formats.

        Returns:
            pd.Timestamp: The matched format string if found, otherwise False.
        """

        for format in formats:
            try:
                timestamp = pd.to_datetime(date_str_to_test, format=format)
                return timestamp  # Match found, return the timestamp
            except (pd.errors.OutOfBoundsDatetime, ValueError):
                pass  # Continue to the next format

        return False  # No match found

    @staticmethod
    def handle_date_parsing(df, date_column):
        """
        Attempts to parse dates in a pandas DataFrame column using two different formats.

        This static method iterates through rows in the DataFrame and tries to
        parse the values in the specified 'date_column' to datetime objects.
        It attempts two different parsing formats:

        1. Primary format (DATE_FMT): This is the preferred format for date parsing.
        2. Secondary format (SECOND_DATE_FMT): If parsing with the primary
            format fails (potentially due to out-of-bounds dates),
            this secondary format is used as a fallback.

        Args:
            df (pd.DataFrame): The DataFrame containing the date column to be parsed.
            date_column (str): The name of the column containing date values.

        Returns:
            pd.DataFrame: The DataFrame with the parsed datetime objects in the
                specified 'date_column' (or original values if parsing fails).
        """
        formats = [DATE_FMT, SECOND_DATE_FMT, THIRD_DATE_FMT]
        drop_index = []
        df = df.reset_index(drop=True)
        for i in range(df.shape[0]):
            date_to_format = df.loc[i, date_column]
            right_format = FormatData.check_date_format(
                date_str_to_test=date_to_format, formats=formats
            )
            if right_format:
                df.loc[i, date_column] = right_format
            else:
                logging.warning(
                    f"""Drop index {date_to_format} Not in formats {formats}🚨
                    """
                )
                drop_index.append(i)
        df = df.drop(drop_index)

        return df
