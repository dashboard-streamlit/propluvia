from dataclasses import dataclass
from typing import Tuple

import pandas as pd


@dataclass
class CheckData:
    """Class CheckData add tools to check data."""

    @staticmethod
    def start_date_upper_end_date(
        df: pd.DataFrame, date_start_col: str, date_end_col: str
    ) -> Tuple[pd.DataFrame]:
        """
        Identifies rows in a DataFrame where the start date is after the
        end date and separates valid and invalid rows.

        This function filters a pandas DataFrame and returns two DataFrames:

        1. invalid_date_rows: A DataFrame containing rows where the value in the
            specified 'date_start_col' is greater than the value in the
            specified 'date_end_col' (start date after end date).
        2. valid_date_rows: A DataFrame containing the remaining rows where the
            date order is valid (start date before or equal to end date).

        Args:
            df (pd.DataFrame): The DataFrame to be processed.
            date_start_col (str): The name of the column containing the start date values.
            date_end_col (str): The name of the column containing the end date values.

        Returns:
            tuple[pd.DataFrame, pd.DataFrame]: A tuple containing two DataFrames:
                invalid_date_rows and valid_date_rows.
        """
        invalid_condition = df[date_start_col] > df[date_end_col]
        invalid_date_rows = df[invalid_condition]
        valid_date_rows = df[~invalid_condition]
        return invalid_date_rows, valid_date_rows
