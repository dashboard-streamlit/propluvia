# Rendu

## Approche

Mon objectif était de rendre l'information accessible aux utilisateurs, même sans connaissances préalables,
en leur permettant de rechercher facilement des données spécifiques.

Pour cela, j'ai découpé les visualisations et les utilisations en pages distinctes.

J'ai privilégié un temps de configuration (téléchargement et formatage) des
données plus long, en amont de l'intégration en base de données,
afin d'éviter de le répéter à chaque requête.

Pour cet exercice, j'ai souhaité présenter les données sous différentes formes :
* métriques
* cartes
* graphiques.

## Expliquez ce que vous voyez et ce que vous apprenez

J'ai découvert l'existence de deux types de zones : "Eaux Superficielles" (SUP) et "Eaux Souterraines" (SOU).

On peut également observer que la dernière année a été fortement impactée par de nombreux arrêtés.

## Avec plus de temps

- [ ] Fixer les pages et les boutons (avec des messages utilisateur) qui ne fonctionnent pas si le premier téléchargement n'a pas été effectué.
- [ ] Optimiser les temps de chargement
- [ ] Améliorer la lisibilité des graphiques en repensant l'organisation des données
- [ ] Proposer des filtres supplémentaires à l'utilisateur
- [ ] Récupérer les données sur une période plus large en les configurant dans ./src/models/config.yaml
- [ ] Poursuivre la feuille de route définie dans le README principal
